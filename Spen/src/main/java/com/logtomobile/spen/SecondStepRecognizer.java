package com.logtomobile.spen;

import android.util.Log;

/**
 * Created by gwiazda on 26.12.13.
 */
public class SecondStepRecognizer {
    public static final float NOTE_OVAL_START_FACTOR = 0.5f;
    public static final float NOTE_FOUR_MINIMAL_FILL_FACTOR = 0.6f;
    public static final int OVAL_INSIDE_PADDING = 2;

    public static boolean isHalfNote(double[] image, int width, int height) {
        int ovalTop = 0;

        for(int i = 0; i < height; i++) {
            int onesCount = 0;
            for(int j = 0; j < width; j++) {
                if(image[(i * width) + j] == 1d) {
                    onesCount++;
                };
            }
            if (((float)onesCount) / ((float)width) > NOTE_OVAL_START_FACTOR) {
                ovalTop = i;
                Log.d("doszlo", "oval top " + i);
                break;
            }
        }

        int blackPoints = 0;
        int ovalInsideTop = ovalTop + OVAL_INSIDE_PADDING;
        int ovalInsideBottom = height - OVAL_INSIDE_PADDING;
        int ovalInsideLeft = OVAL_INSIDE_PADDING;
        int ovalInsideRight = width - OVAL_INSIDE_PADDING;

        for(int i = ovalInsideTop; i < ovalInsideBottom; i++) {
            for(int j = ovalInsideLeft; j < ovalInsideRight; j++) {
                if(image[(i * width) + j] == 1d) {
                    blackPoints++;
                };
            }
        }

        if (((float) blackPoints) / ((float) (ovalInsideBottom - ovalInsideTop) * (ovalInsideRight - ovalInsideLeft)) < NOTE_FOUR_MINIMAL_FILL_FACTOR) {
            return true;
        } else {
            return false;
        }
    }
}
