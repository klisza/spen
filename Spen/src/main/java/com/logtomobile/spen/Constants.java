package com.logtomobile.spen;

public class Constants {	
	public static final int DOWNSAMPLE_WIDTH = 15;
	public static final int DOWNSAMPLE_HEIGHT = 20;
	
	public static final String NEURAL_NETWORK_FILENAME = "neural_network_10x12";
	public static final String LEARNING_SEQUENCE_FILENAME = "learning_sequence";
}