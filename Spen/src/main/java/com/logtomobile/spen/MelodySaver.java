package com.logtomobile.spen;

import android.content.Context;
import android.os.Environment;

import com.google.common.collect.Lists;
import com.logtomobile.spen.stave_music.Note;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

/**
 * Created by gwiazda on 23.12.13.
 */
public class MelodySaver {
    public static final String GRID_MUSIC_DIR_NAME = "grid_music";
    public static final String STAVE_MUSIC_DIR_NAME = "stave_music";


    public static void saveGridMusicSample(Context context, String name, InstrumentType[][] gridMusicMatrix) {
        String generatedUUID = UUID.randomUUID().toString();
        String filesDir = context.getFilesDir() + File.separator + GRID_MUSIC_DIR_NAME;

        File dir = new File(filesDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        File file = new File(filesDir + File.separator + generatedUUID);

        GridMusicSample sample = new GridMusicSample(generatedUUID, name, gridMusicMatrix);

        try {
            FileOutputStream fOut = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fOut);

            out.writeObject(sample);
            out.close();
        } catch (IOException ioe) {
            throw new NullPointerException();
        }
    }

    public static void saveStaveMusicSample(Context context, String name, List<Note.NoteTrack> noteTracks) {
        String generatedUUID = UUID.randomUUID().toString();
        String filesDir = context.getFilesDir() + File.separator + STAVE_MUSIC_DIR_NAME;

        File dir = new File(filesDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        File file = new File(filesDir + File.separator + generatedUUID);

        StaveMusicSample sample = new StaveMusicSample(generatedUUID, name, noteTracks);

        try {
            FileOutputStream fOut = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fOut);

            out.writeObject(sample);
            out.close();
        } catch (IOException ioe) {
            throw new NullPointerException();
        }
    }

    public static List<StaveMusicSample> getSavedStaveSamples(Context context) {
        List<StaveMusicSample> samples = Lists.newArrayList();
        File dir = new File(context.getFilesDir() + File.separator + STAVE_MUSIC_DIR_NAME);
        if (dir.exists()) {
            for (File file : dir.listFiles()) {
                try {
                    FileInputStream fIn = new FileInputStream(file);
                    ObjectInputStream in = new ObjectInputStream(fIn);

                    StaveMusicSample sample = (StaveMusicSample) in.readObject();
                    in.close();
                    samples.add(sample);
                } catch (FileNotFoundException fnfe) {
                    //no need to handle
                } catch (IOException ioe) {
                    //no need to handle
                } catch (ClassNotFoundException cnfe) {
                    //no need to handle
                }
            }
        }
        return samples;
    }

    public static List<GridMusicSample> getSavedGridSamples(Context context) {
        List<GridMusicSample> samples = Lists.newArrayList();
        File dir = new File(context.getFilesDir() + File.separator + GRID_MUSIC_DIR_NAME);
        if (dir.exists()) {
            for (File file : dir.listFiles()) {
                try {
                    FileInputStream fIn = new FileInputStream(file);
                    ObjectInputStream in = new ObjectInputStream(fIn);

                    GridMusicSample sample = (GridMusicSample) in.readObject();
                    in.close();
                    samples.add(sample);
                } catch (FileNotFoundException fnfe) {
                    //no need to handle
                } catch (IOException ioe) {
                    //no need to handle
                } catch (ClassNotFoundException cnfe) {
                    //no need to handle
                }
            }
        }
        return samples;
    }

    public static class StaveMusicSample extends MusicSample implements Serializable{
        private static final long serialVersionUID = 45687659750857L;
        private String mUUID;
        private String mName;
        private List<Note.NoteTrack> mNoteTracks;

        private StaveMusicSample() {}

        public StaveMusicSample(String UUID, String name, List<Note.NoteTrack> noteTracks) {
            mUUID = UUID;
            mName = name;
            mNoteTracks = noteTracks;
        }

        public String getUUID() {
            return mUUID;
        }

        public String getName() {
           return mName;
        }

        public List<Note.NoteTrack> getNoteTrack() {
            return mNoteTracks;
        }

        public String getFilePath(Context context) {
            return context.getFilesDir() + File.separator + STAVE_MUSIC_DIR_NAME+ File.separator + mUUID;
        }
    }

    public static class GridMusicSample extends MusicSample implements Serializable{
        private static final long serialVersionUID = 909875452389765038L;
        private String mUUID;
        private String mName;
        private InstrumentType[][] mGridMusicMatrix;

        private GridMusicSample() {}

        public GridMusicSample(String UUID, String name, InstrumentType[][] gridMusicMatrix) {
            mUUID = UUID;
            mName = name;
            mGridMusicMatrix = gridMusicMatrix;
        }

        public String getUID() {
            return mUUID;
        }

        public String getName() {
            return mName;
        }

        public InstrumentType[][] getMusicMatrix() {
            return mGridMusicMatrix;
        }

        public String getFilePath(Context context) {
            return context.getFilesDir() + File.separator + GRID_MUSIC_DIR_NAME + File.separator + mUUID;
        }
    }

    public static abstract class MusicSample implements Serializable{
        public abstract String getName();
        public abstract String getFilePath(Context context);
    }
}
