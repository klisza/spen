package com.logtomobile.spen;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by gwiazda on 13.12.13.
 */
public enum InstrumentType implements Serializable{
    Trumpet,
    Guitar,
    Violin,
    Piano,
    Drums;

    private static final long serialVersionUID = 6760889754976563597L;
    private static HashMap<InstrumentType, Integer> mColorsMap;
    private static HashMap<InstrumentType, Integer> mFileNamesMap;

    static {
        mFileNamesMap = new HashMap<InstrumentType, Integer>();

        mFileNamesMap.put(Trumpet, R.raw.trumpet);
        mFileNamesMap.put(Guitar, R.raw.guitar);
        mFileNamesMap.put(Violin, R.raw.violin);
        mFileNamesMap.put(Piano, R.raw.piano);
        mFileNamesMap.put(Drums, R.raw.drums);
    }

    public int getInstrumentColor(Context context) {
        if (mColorsMap == null) {
            mColorsMap = new HashMap<InstrumentType, Integer>();
            Resources res = context.getResources();
            mColorsMap.put(Trumpet, res.getColor(R.color.trumpet_color));
            mColorsMap.put(Guitar, res.getColor(R.color.guitar_color));
            mColorsMap.put(Violin, res.getColor(R.color.violin_color));
            mColorsMap.put(Piano, res.getColor(R.color.piano_color));
            mColorsMap.put(Drums, res.getColor(R.color.drums_color));

        }
        return mColorsMap.get(this);
    }

    public int getResourceID() {
        return mFileNamesMap.get(this);
    }
}
