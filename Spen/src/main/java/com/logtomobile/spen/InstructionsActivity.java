package com.logtomobile.spen;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by gwiazda on 30.12.13.
 */
public class InstructionsActivity extends Activity {

    private TextView mTxtvHeader;
    private TextView mTxtvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.instructions_activity);

        mTxtvHeader = (TextView) findViewById(R.id.txtvHeader);
        FontSetter.setFont(this, mTxtvHeader);
        mTxtvTitle = (TextView) findViewById(R.id.txtvTitle);
        FontSetter.setFont(this, mTxtvTitle);

    }
}
