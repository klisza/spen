package com.logtomobile.spen;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by gwiazda on 29.12.13.
 */
public class FirstActivity extends Activity {
    private static final String FIRST_USE = "first_use";
    private TextView mTxtvBoard;
    private TextView mTxtvStave;
    private TextView mTxtvInstructions;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_first);

        findViews();
        setListeners();
        setFonts();
        loadFilesFromAssets();
    }

    private void loadFilesFromAssets() {
        SharedPreferences prefs = this.getSharedPreferences("com.logtomobile.spen", Context.MODE_PRIVATE);
        if (!prefs.contains(FIRST_USE)) {
            copyFile(this, "1f2baa6e-011d-430c-8575-4a8bcefeb33c", getFilesDir() + File.separator + MelodySaver.STAVE_MUSIC_DIR_NAME + File.separator +  "1f2baa6e-011d-430c-8575-4a8bcefeb33c");
            copyFile(this, "87a3fb18-ea15-4961-ba4f-f2a5d4266753", getFilesDir() + File.separator + MelodySaver.GRID_MUSIC_DIR_NAME + File.separator + "87a3fb18-ea15-4961-ba4f-f2a5d4266753");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean(FIRST_USE, true);
            editor.commit();
        }
    }

    private boolean copyFile(Context context, String sourceFileName, String destFileName)
    {
        AssetManager assetManager = context.getAssets();

        File destFile = new File(destFileName);

        File destParentDir = destFile.getParentFile();
        destParentDir.mkdir();

        InputStream in = null;
        OutputStream out = null;
        try
        {
            in = assetManager.open(sourceFileName);
            out = new FileOutputStream(destFile);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1)
            {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;

            return true;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    private void findViews() {
        mTxtvBoard = (TextView) findViewById(R.id.txtvBoard);
        mTxtvStave = (TextView) findViewById(R.id.txtvStave);
        mTxtvInstructions = (TextView) findViewById(R.id.txtvInstructions);
    }

    private void setListeners() {
        mTxtvBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, MainActivity.class);
                intent.putExtra(MainActivity.INTENT_EXTRA_CANVAS_MODE_STAVE, false);
                startActivity(intent);
            }
        });

        mTxtvStave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, MainActivity.class);
                intent.putExtra(MainActivity.INTENT_EXTRA_CANVAS_MODE_STAVE, true);
                startActivity(intent);
            }
        });

        mTxtvInstructions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FirstActivity.this, InstructionsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setFonts() {
        FontSetter.setFont(this, mTxtvBoard);
        FontSetter.setFont(this, mTxtvStave);
        FontSetter.setFont(this, mTxtvInstructions);
    }
}
