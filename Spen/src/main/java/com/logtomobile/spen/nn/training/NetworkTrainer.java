package com.logtomobile.spen.nn.training;

import java.util.Collections;
import java.util.List;

import com.logtomobile.spen.nn.NeuralNetwork;

public class NetworkTrainer {
	private static final double EPSILON = 0.05f;
	public static final int MAX_EPOCH_NUMBER = 1000;
    private volatile boolean isCurrentlyTraining;
	
	private List<LearningSample> learningSequence;
	private NeuralNetwork nn;
	
	public NetworkTrainer(NeuralNetwork nn, List<LearningSample> learningSequence) {
		this.nn = nn;
		this.learningSequence = learningSequence;
	}
	
	public void train(OnProgressChangedListener progressListener) {
		if (!isCurrentlyTraining) {
            isCurrentlyTraining = true;
            double res[];
            double error[];
            boolean trained = false;

            System.out.println("learning sequence size: " + learningSequence.size());

            int epochs = -1;
            double netError = 0.0;

            while (!trained && epochs < MAX_EPOCH_NUMBER) {
                ++epochs;

                trained = true;
                netError = 0.0f;

                for (LearningSample sample : learningSequence) {
                    res = nn.computeOutput(sample.input);
                    error = sample.computeError(res);

                    for (double e : error)
                        netError += 0.5 * e * e;

                    nn.backpropagate(error);
                }

                if (netError > EPSILON)
                    trained = false;
                else Collections.shuffle(learningSequence);

                if (progressListener != null) {
                    if (trained)
                        progressListener.onProgressChanged(MAX_EPOCH_NUMBER, netError);
                    else progressListener.onProgressChanged(epochs, netError);
                }
            }

            System.out.println("epochs: " + epochs);
            isCurrentlyTraining = false;
        }
	}
}