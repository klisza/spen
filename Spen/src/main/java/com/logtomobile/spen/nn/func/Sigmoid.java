package com.logtomobile.spen.nn.func;

import java.io.Serializable;

public class Sigmoid implements ActivationFunction, Serializable {
	private static final long serialVersionUID = 2568909211080332733L;
	
	private static Sigmoid func;
	
	private Sigmoid() {}
	
	public static Sigmoid getInstance() {
		if (func == null)
			func = new Sigmoid();
		
		return func;
	}

	@Override
	public double func(double x) {
		return 1.0 / (1.0 + Math.exp(-x));
	}

	@Override
	public double firstDerivative(double x) {
		return func(x) * (1.0 - (1.0 / (1.0 + Math.exp(-x))));
	}
}