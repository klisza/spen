package com.logtomobile.spen.nn;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;

import com.logtomobile.spen.nn.func.Sigmoid;
import com.logtomobile.spen.nn.training.LearningSample;
import com.logtomobile.spen.nn.training.NetworkTrainer;
import com.logtomobile.spen.nn.training.OnProgressChangedListener;

import android.annotation.SuppressLint;
import android.content.Context;

public class CharacterRecognizer {
	private static CharacterRecognizer recognizer;
	
	private NeuralNetwork nn;
	private LinkedList<LearningSample> learningSequence = new LinkedList<LearningSample>();
	
	@SuppressLint("UseSparseArrays")
	private HashMap<Integer, Character> outputMap = new HashMap<Integer, Character>();
	
	private void normalizeOutput(double out[]) {
		int maxIndex = 0;
		double max = out[0];
		
		for (int i = 1; i < out.length; ++i) {
			if (out[i] > max) {
				maxIndex = i;
				max = out[i];
			}
		}
		
		Arrays.fill(out, 0.0f);
		if (max > 0.25)
			out[maxIndex] = 1.0f;
	}
	
	private void initNetwork(Context context) {
        outputMap.put(Arrays.hashCode(new double[] {0.0, 0.0, 0.0}), '0');
        outputMap.put(Arrays.hashCode(new double[] {1.0, 0.0, 0.0}), '1');
        outputMap.put(Arrays.hashCode(new double[] {0.0, 1.0, 0.0}), '2');
        outputMap.put(Arrays.hashCode(new double[] {0.0, 0.0, 1.0}), '3');


        int inputs = com.logtomobile.spen.Constants.DOWNSAMPLE_WIDTH * com.logtomobile.spen.Constants.DOWNSAMPLE_HEIGHT;
        
		LayerParams inputLayer = new LayerParams(inputs, 1, Sigmoid.getInstance(), true);
		LayerParams hiddenLayer = new LayerParams(inputs, inputs, Sigmoid.getInstance(), false);
		LayerParams outputLayer = new LayerParams(outputMap.size() - 1, inputs, Sigmoid.getInstance(), false);

        //nn = new NeuralNetwork(inputLayer, hiddenLayer, outputLayer);

        try {
            ObjectInputStream in = new ObjectInputStream(context.getAssets().open("neural_network_one_four_disabled"));

            nn = (NeuralNetwork) in.readObject();
            in.close();
        } catch (IOException ioe) {
            throw new NullPointerException();
        } catch (ClassNotFoundException cnfe) {
            throw new NullPointerException();
        }

	}
	
	private CharacterRecognizer(Context context) {
		initNetwork(context);
	}
	
	public static CharacterRecognizer getInstance(Context context) {
		if (recognizer == null)
			recognizer = new CharacterRecognizer(context.getApplicationContext());
		
		return recognizer;
	}
	
	public void train(final OnProgressChangedListener progressListener) {
		Thread trainingThread = new Thread() {
			@Override
			public void run() {
				NetworkTrainer trainer = new NetworkTrainer(nn, learningSequence);
				trainer.train(progressListener);
			}
		};
		trainingThread.start();
	}
	
	public void addLearningSample(LearningSample sample) {
		learningSequence.add(sample);
	}
	
	public char recognize(double input[]) {
		double out[] = nn.computeOutput(input);

		normalizeOutput(out);
		
		Character c = outputMap.get(Arrays.hashCode(out));
		
		if (c == null) return '1';
		else return c;
	}
	
	public NeuralNetwork getNeuralNetwork() {
		return nn;
	}
	
	public void setNeuralNetwork(NeuralNetwork nn) {
		this.nn = nn;
	}
	
	public LinkedList<LearningSample> getLearningSequence() {
		return learningSequence;
	}
	
	public void setLearningSequence(LinkedList<LearningSample> sequence) {
		this.learningSequence = sequence;
	}
}