package com.logtomobile.spen.nn;

import static com.logtomobile.spen.nn.Constants.DEFAULT_ACTIVATION_FUNCTION;
import static com.logtomobile.spen.nn.Constants.DEFAULT_LEARNING_COEFFICIENT;

import java.io.Serializable;

import com.logtomobile.spen.nn.func.ActivationFunction;

public class LayerParams implements Serializable {
	private static final long serialVersionUID = 49060603978481522L;
	
	int neuronsCount;
	int neuronInputsCount;
	double learningCoefficient;
	boolean inputLayer;
	
	ActivationFunction activationFunc;
	
	public LayerParams(int neuronsCount, int neuronInputsCount, double learningCoefficient, ActivationFunction activationFunc, boolean inputLayer) {
		this.neuronsCount = neuronsCount;
		this.neuronInputsCount = neuronInputsCount;
		this.learningCoefficient = learningCoefficient;
		this.activationFunc = activationFunc;
		this.inputLayer = inputLayer;
	}
	
	public LayerParams(int neuronsCount, int neuronInputsCount, double learningCoefficient, boolean inputLayer) {
		this(neuronsCount, neuronInputsCount, learningCoefficient, DEFAULT_ACTIVATION_FUNCTION, inputLayer);
	}
	
	public LayerParams(int neuronsCount, int neuronInputsCount, boolean inputLayer) {
		this(neuronsCount, neuronInputsCount, DEFAULT_LEARNING_COEFFICIENT, DEFAULT_ACTIVATION_FUNCTION, inputLayer);
	}
	
	public LayerParams(int neuronsCount, int neuronInputsCount, ActivationFunction activationFunc, boolean inputLayer) {
		this(neuronsCount, neuronInputsCount, DEFAULT_LEARNING_COEFFICIENT, activationFunc, inputLayer);
	}
}