package com.logtomobile.spen.nn;

import static com.logtomobile.spen.nn.Constants.DEFAULT_ACTIVATION_FUNCTION;
import static com.logtomobile.spen.nn.Constants.DEFAULT_LEARNING_COEFFICIENT;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.logtomobile.spen.nn.func.ActivationFunction;

public class Layer implements Serializable {
    private static final long serialVersionUID = -6244671342112062282L;

    protected List<Neuron> neurons;
    protected double lastOutput[];

    protected int neuronInputsCount;
    protected boolean inputLayer = false;

    public Layer(int neuronsCount, int neuronInputsCount, double learningCoefficient, ActivationFunction func, boolean inputLayer) {
        if (neuronsCount <= 0)
            throw new IllegalArgumentException("neurons count <= 0");

        if (neuronInputsCount <= 0)
            throw new IllegalArgumentException("neuron inputs count <= 0");

        if (learningCoefficient <= 0.0)
            throw new IllegalArgumentException("learning coefficient <= 0");

        neurons = new ArrayList<Neuron>();
        lastOutput = new double[neuronsCount];

        for (int i = 0; i < neuronsCount; ++i)
            neurons.add(new Neuron(neuronInputsCount, learningCoefficient, func));

        this.neuronInputsCount = neuronInputsCount;
        this.inputLayer = inputLayer;
    }

    public Layer(int neuronsCount, int neuronInputsCount, boolean inputLayer) {
        this(neuronsCount, neuronInputsCount, DEFAULT_LEARNING_COEFFICIENT, DEFAULT_ACTIVATION_FUNCTION, inputLayer);
    }

    public Layer(LayerParams params) {
        this(params.neuronsCount, params.neuronInputsCount, params.learningCoefficient, params.activationFunc, params.inputLayer);
    }

    public double[] computeOutput(double input[]) {
        if (inputLayer) {
            for (int i = 0; i < neurons.size(); ++i)
                lastOutput[i] = neurons.get(i).computeOutput(new double[]{input[i]});
        }
        else {
            for (int i = 0; i < neurons.size(); ++i)
                lastOutput[i] = neurons.get(i).computeOutput(input);
        }

        return lastOutput;
    }

    public double[] computeDelta(double d[]) {
        if (d.length != neurons.size())
            throw new IllegalArgumentException("d.length != neurons count");

        double delta[] = new double[neuronInputsCount];
        double weights[];

        for (int n = 0; n < neurons.size(); ++n) {
            weights = neurons.get(n).weights;

            for (int i = 0; i < weights.length; ++i)
                delta[i] += d[n] * weights[i];
        }

        return delta;
    }

    public void correctWeights(double delta[]) {
        if (delta.length != neurons.size())
            throw new IllegalArgumentException("delta.length != neurons.length");

        for (int n = 0; n < neurons.size(); ++n)
            neurons.get(n).correctWeights(delta[n]);
    }
}