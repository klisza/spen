package com.logtomobile.spen.nn.training;

public interface OnProgressChangedListener {
	void onProgressChanged(int progress, double error);
}