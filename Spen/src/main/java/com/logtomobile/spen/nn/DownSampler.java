package com.logtomobile.spen.nn;

import android.graphics.Bitmap;

/**
 * Created by gwiazda on 16.12.13.
 */
public class DownSampler {
    protected static final float FILL_FACTOR = 0.4f;
    public static final int DOWNSAMPLE_WIDTH = 15;
    public static final int DOWNSAMPLE_HEIGHT = 20;

    protected static int downSampleLeft, downSampleRight;
    protected static int downSampleTop, downSampleBottom;
    protected static double ratioX, ratioY;
    protected static int pixelMap[];

    protected static boolean hLineClear(Bitmap img, int y, int drawingColor) {
        int w = img.getWidth();

        for (int i = 0; i < w; i++) {
            if (pixelMap[(y * w) + i] != 0)
                return false;
        }

        return true;
    }

    protected static boolean vLineClear(Bitmap img, int x, int drawingColor) {
        int w = img.getWidth();
        int h = img.getHeight();

        for (int i = 0; i < h; i++) {
            if (pixelMap[(i * w) + x] != 0)
                return false;
        }

        return true;
    }

    protected static void findBounds(Bitmap img, int w, int h, int drawingColor) {
        // top line
        for (int y = 0; y < h; y++) {
            if (!hLineClear(img, y, drawingColor)) {
                downSampleTop = y;
                break;
            }

        }
        // bottom line
        for (int y = h - 1; y >= 0; y--) {
            if (!hLineClear(img, y, drawingColor)) {
                downSampleBottom = y;
                break;
            }
        }
        // left line
        for (int x = 0; x < w; x++) {
            if (!vLineClear(img, x, drawingColor)) {
                downSampleLeft = x;
                break;
            }
        }

        // right line
        for (int x = w - 1; x >= 0; x--) {
            if (!vLineClear(img, x, drawingColor)) {
                downSampleRight = x;
                break;
            }
        }
    }

    protected static boolean downSampleQuadrant(Bitmap img, int x, int y, int drawingColor) {
        int pixelMapLength = pixelMap.length;
        int w = img.getWidth();
        int startX = (int) (downSampleLeft + (x * ratioX));
        int startY = (int) (downSampleTop + (y * ratioY));
        int endX = (int) (startX + ratioX);
        int endY = (int) (startY + ratioY);

        int filledPointsCount = 0;
        for (int yy = startY; yy <= endY; yy++) {
            for (int xx = startX; xx <= endX; xx++) {
                int loc = xx + (yy * w);

                if (loc < pixelMapLength && pixelMap[loc] != 0)
                    filledPointsCount++;
            }

        }
        if (((float) filledPointsCount) / ((float) (endX - startX) * (endY - startY)) > FILL_FACTOR) {
            return true;
        } else {
            return false;
        }
    }

    public static synchronized double[] processImage(Bitmap img, int drawingColor) {
        int w = img.getWidth();
        int h = img.getHeight();

        double ds[] = new double[DOWNSAMPLE_WIDTH * DOWNSAMPLE_HEIGHT];

        pixelMap = new int[img.getWidth() * img.getHeight()];
        img.getPixels(pixelMap, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        findBounds(img, w, h, drawingColor);

        ratioX = (double) (downSampleRight - downSampleLeft) / (double)DOWNSAMPLE_WIDTH;
        ratioY = (double) (downSampleBottom - downSampleTop) / (double)DOWNSAMPLE_HEIGHT;

        int arrayIndex = 0;
        for (int y = 0; y < DOWNSAMPLE_HEIGHT; y++) {
            for (int x = 0; x < DOWNSAMPLE_WIDTH; x++) {
                if (downSampleQuadrant(img, x, y, drawingColor))
                    ds[arrayIndex++] = 1.0f;
                else ds[arrayIndex++] = 0.0f;
            }
        }

        return ds;
    }
}
