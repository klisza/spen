package com.logtomobile.spen.nn.training;

import java.io.Serializable;

public final class LearningSample implements Serializable {
	private static final long serialVersionUID = -7449954283925183829L;
	
	double input[];
	double targetOutput[];
	
	public LearningSample(double input[], double targetOutput[]) {
		this.input = input;
		this.targetOutput = targetOutput;
	}
	
	public double[] computeError(double output[]) {		
		double error[] = new double[output.length];
		
		for (int i = 0; i < output.length; ++i)
			error[i] = targetOutput[i] - output[i];
		
		return error;
	}
	
	public double[] getInput() {
		return input;
	}
	
	public double[] getTargetOutput() {
		return targetOutput;
	}
}