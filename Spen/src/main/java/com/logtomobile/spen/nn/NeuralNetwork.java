package com.logtomobile.spen.nn;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NeuralNetwork implements Serializable {
    private static final long serialVersionUID = 6420494911755839309L;

    protected List<Layer> layers;

    public NeuralNetwork(LayerParams... params) {
        layers = new ArrayList<Layer>();

        for (int i = 0; i < params.length; ++i)
            layers.add(new Layer(params[i]));
    }

    public double[] computeOutput(double input[]) {
        double inp[];

        inp = layers.get(0).computeOutput(input);

        for (int i = 1; i < layers.size(); ++i)
            inp = layers.get(i).computeOutput(inp);

        return layers.get(layers.size() - 1).lastOutput;
    }

    public void backpropagate(double outputError[]) {
        double d[] = outputError;

        for (int i = layers.size() - 1; i >= 1; --i) {
            layers.get(i).correctWeights(d);
            d = layers.get(i).computeDelta(d);
        }
    }
}