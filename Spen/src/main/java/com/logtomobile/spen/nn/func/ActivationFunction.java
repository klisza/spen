package com.logtomobile.spen.nn.func;

public interface ActivationFunction {
	public double func(double x);
	public double firstDerivative(double x);
}