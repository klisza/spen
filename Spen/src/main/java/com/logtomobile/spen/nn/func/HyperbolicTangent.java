package com.logtomobile.spen.nn.func;

import java.io.Serializable;

public final class HyperbolicTangent implements ActivationFunction, Serializable {
	private static final long serialVersionUID = 1273431505383826388L;
	
	private static HyperbolicTangent func;
	
	private HyperbolicTangent() {}
	
	public static HyperbolicTangent getInstance() {
		if (func == null)
			func = new HyperbolicTangent();
		
		return func;
	}

	@Override
	public double func(double x) {
		return Math.tanh(x);
	}

	@Override
	public double firstDerivative(double x) {
		return 1.0 * (1.0 - Math.tanh(x) * Math.tanh(x));
	}
}