package com.logtomobile.spen.nn;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by gwiazda on 17.12.13.
 */
public class Serializer {
    private static Serializer serializer;

    private Serializer() {}

    public static Serializer getInstance() {
        if (serializer == null)
            serializer = new Serializer();

        return serializer;
    }

    public void saveToExternalStorage(final Serializable obj, String fileName) throws IOException {
        FileOutputStream fOut = new FileOutputStream(new File(fileName));
        ObjectOutputStream out = new ObjectOutputStream(fOut);

        out.writeObject(obj);
        out.close();
    }

    public void save(Context context, final Serializable obj, String fileName) throws IOException {
        Context ctx = context.getApplicationContext();
        FileOutputStream fOut = ctx.openFileOutput(fileName, Context.MODE_PRIVATE);
        ObjectOutputStream out = new ObjectOutputStream(fOut);

        out.writeObject(obj);
        out.close();
    }

    public Object load(Context context, String fileName) throws IOException, ClassNotFoundException {
        Context ctx = context.getApplicationContext();
        FileInputStream fIn = ctx.openFileInput(fileName);
        ObjectInputStream in = new ObjectInputStream(fIn);

        Object obj = in.readObject();
        in.close();

        return obj;
    }
}
