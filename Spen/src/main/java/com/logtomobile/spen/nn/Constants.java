package com.logtomobile.spen.nn;

import com.logtomobile.spen.nn.func.ActivationFunction;
import com.logtomobile.spen.nn.func.Sigmoid;

public final class Constants {
	public static final double DEFAULT_LEARNING_COEFFICIENT = 0.3;
	public static final double MIN_INITIAL_WEIGHT_VALUE = -1.0;
	public static final double MAX_INITIAL_WEIGHT_VALUE = 1.0;
	
	public static final ActivationFunction DEFAULT_ACTIVATION_FUNCTION = Sigmoid.getInstance();
}