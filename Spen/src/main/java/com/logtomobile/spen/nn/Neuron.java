package com.logtomobile.spen.nn;

import static com.logtomobile.spen.nn.Constants.DEFAULT_ACTIVATION_FUNCTION;
import static com.logtomobile.spen.nn.Constants.DEFAULT_LEARNING_COEFFICIENT;
import static com.logtomobile.spen.nn.Constants.MAX_INITIAL_WEIGHT_VALUE;
import static com.logtomobile.spen.nn.Constants.MIN_INITIAL_WEIGHT_VALUE;

import java.io.Serializable;
import java.util.Random;

import com.logtomobile.spen.nn.func.ActivationFunction;

public class Neuron implements Serializable {
	private static final long serialVersionUID = -2866231378072078456L;
	
	protected double bias = 1.0f;
	protected double biasWeight;
	
	protected double learningCoefficient;
	protected double lastOutput;
	protected double lastSum;
	
	protected double weights[];
	protected double input[];
	
	protected ActivationFunction activationFunc;
	
	protected static Random rand = new Random(System.currentTimeMillis());
	
	protected void generateRandomWeights() {
		double diff = MAX_INITIAL_WEIGHT_VALUE - MIN_INITIAL_WEIGHT_VALUE;
		
		for (int i = 0; i < weights.length; ++i)
			weights[i] = MIN_INITIAL_WEIGHT_VALUE + rand.nextFloat() * diff;
		
		biasWeight = MIN_INITIAL_WEIGHT_VALUE + rand.nextFloat() * diff;
	}
	
	public Neuron(int inputsCount, double learningCoefficient, ActivationFunction activationFunc) {
		if (inputsCount <= 0)
			throw new IllegalArgumentException("inputs count <= 0");
		
		weights = new double[inputsCount];
		input = new double[inputsCount];
		
		this.activationFunc = activationFunc;
		this.learningCoefficient = learningCoefficient;
		
		generateRandomWeights();
	}
	
	public Neuron(int inputsCount, ActivationFunction activationFunc) {
		this(inputsCount, DEFAULT_LEARNING_COEFFICIENT, activationFunc);
	}
	
	public Neuron(int inputsCount) {
		this(inputsCount, DEFAULT_LEARNING_COEFFICIENT, DEFAULT_ACTIVATION_FUNCTION);
	}
	
	public double computeOutput(double input[]) {
		if (input.length != weights.length)
			throw new IllegalArgumentException("weights count != inputs count");
		
		this.input = input;
		
		double res = 0.0f;
		
		for (int i = 0; i < input.length; ++i)
			res += weights[i] * input[i];
		
		res += bias * biasWeight;
		
		lastSum = res;
		res = activationFunc.func(res);
		lastOutput = res;
		
		return res;
	}
	
	public double computeOutput() {
		return computeOutput(this.input);
	}
	
	public void correctWeights(double delta) {		
		for (int i = 0; i < weights.length; ++i)
			weights[i] += learningCoefficient * delta * activationFunc.firstDerivative(lastSum) * input[i];
		
		biasWeight += learningCoefficient * delta * activationFunc.firstDerivative(lastSum) * bias;
	}
}