package com.logtomobile.spen;

import java.io.Serializable;

/**
 * Created by gwiazda on 13.12.13.
 */
public interface MusicFragmentInterface {
    public void play();
    public void stop();
    public void setInstrument(InstrumentType type);
    public boolean saveMusicTrack(String trackName);
    public boolean setErase();
    public void clean();
    public void loadSample(Object sample);
    public Serializable getDrawnSample();
}
