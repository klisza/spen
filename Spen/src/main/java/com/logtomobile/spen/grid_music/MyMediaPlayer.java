package com.logtomobile.spen.grid_music;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

import com.logtomobile.spen.InstrumentType;
import com.logtomobile.spen.MusicDrawer;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by gwiazda on 12.12.13.
 */
public class MyMediaPlayer {
    private static double mToneFrequencyMultiplier = Math.pow(2, 1d/7d);
    private Thread mMainPlayingThread;

    private HashMap<InstrumentType, Integer> mPoolSoundIdMap = new HashMap<InstrumentType, Integer>();
    private HashSet<Integer> mCurrentStreamsSet = new HashSet<Integer>();

    private int mScale;
    private int mTimeDivider;
    private long mDuration;

    private long mToneDuration;

    private Context mContext;
    private InstrumentType[][] mMusicMatrix;
    private SoundPool mSoundPool;

    public MyMediaPlayer(Context context, long duration) {
        mContext = context.getApplicationContext();
        mDuration = duration;
        mSoundPool = new SoundPool(40, AudioManager.STREAM_MUSIC, 100);

        loadSounds();
    }

    private void loadSounds() {
        for (InstrumentType instrument : InstrumentType.values()) {
            mPoolSoundIdMap.put(instrument, mSoundPool.load(mContext, instrument.getResourceID(), 1));
        }
    }

    public void setMusicMatrix(InstrumentType[][] musicMatrix) {
        mTimeDivider = musicMatrix.length;
        mScale = musicMatrix[0].length;
        mToneDuration = mDuration / ((long) mTimeDivider) ;
        mMusicMatrix = musicMatrix;
    }

    public synchronized void stopPlaying() {
        if (mMainPlayingThread != null && mMainPlayingThread.isAlive()) {
            mMainPlayingThread.interrupt();
        }
        for (Integer streamId : mCurrentStreamsSet) {
            mSoundPool.stop(streamId);
        }
        mCurrentStreamsSet.clear();
    }

    public void startPlaying(final MusicDrawer drawer, final boolean mergeRects) {
        if (mMusicMatrix != null) {
            if (mMainPlayingThread != null && mMainPlayingThread.isAlive()) {
                mMainPlayingThread.interrupt();
            }
            mMainPlayingThread = new Thread(new Runnable() {
                @Override
                public void run() {

                    final int width = mMusicMatrix.length;
                    for (int i = 0; i < width; i++) {
                        final int finalI = i;
                        final int iter = i;
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                InstrumentType[] column = mMusicMatrix[iter];
                                for (int j = 0; j < column.length; j++) {
                                    InstrumentType type = column[j];

                                    if (type != null) {
                                        int multiplier = 1;
                                        if (mergeRects && (finalI + multiplier) < width) {
                                            InstrumentType currentInstrument = mMusicMatrix[finalI + multiplier][j];
                                            while (currentInstrument == type && (finalI + multiplier) < width) {
                                                currentInstrument = mMusicMatrix[finalI + multiplier][j];
                                                mMusicMatrix[finalI + multiplier++][j] = null;
                                            }
                                        }
                                        playSound(type, mToneDuration * multiplier, mScale - j - 1);
                                    }
                                }
                            }
                        }).start();
                        try {
                            Thread.sleep(mToneDuration);
                        } catch (InterruptedException ie) {
                            break;
                        }
                    }

                    drawer.enableDraw();
                }
            });
            mMainPlayingThread.start();
        }
    }

    private void playSound(InstrumentType instrumentType, final long duration, int level) {
        if (level >= 0 && level < mScale) {
            float countedRate = countPlayRate(level);
            final Integer currentStreamID = mSoundPool.play(mPoolSoundIdMap.get(instrumentType), 1f, 1f, 1, 0, countedRate);
            mCurrentStreamsSet.add(currentStreamID);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(duration);
                        mCurrentStreamsSet.remove(currentStreamID);
                        mSoundPool.stop(currentStreamID);
                    } catch (InterruptedException ie) {
                        mCurrentStreamsSet.remove(currentStreamID);
                       mSoundPool.stop(currentStreamID);
                    }

                }
            }).start();
        }
    }

    private float countPlayRate(int level) {
        return ((float) Math.pow(mToneFrequencyMultiplier, (double) (level - 7)));
    }
}
