package com.logtomobile.spen.grid_music;

import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.logtomobile.spen.InstrumentType;
import com.logtomobile.spen.MelodySaver;
import com.logtomobile.spen.MusicFragmentInterface;
import com.logtomobile.spen.R;
import com.samsung.samm.common.SObject;
import com.samsung.spensdk.applistener.SCanvasInitializeListener;
import com.samsung.spensdk.applistener.SCanvasLongPressListener;
import com.samsung.spensdk.applistener.SObjectSelectListener;

import java.io.Serializable;
import java.util.List;

/**
 * Created by gwiazda on 13.12.13.
 */
public class GridMusicFragment extends Fragment implements MusicFragmentInterface {
    private View mRootView;
    private RelativeLayout mRlCanvasHolder;
    private ImageView mImgvLine;

    private ObjectAnimator mLineAnimator;
    private GridMusicCanvas mSCanvasView;
    private MyMediaPlayer mMediaPlayer;

    private long mDuration;
    private int mCurrentToneScale;
    private int mCurrentTimeDivide;

    private Object mSampleToLoad;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_grid_music, container,  false);

        findViews();

        mCurrentToneScale = 15;
        mCurrentTimeDivide = 60;
        mDuration = 20000;

        GridMusicCanvas.Params.Builder builder = new GridMusicCanvas.Params.Builder();
        builder.linesHorisontal(mCurrentTimeDivide);
        builder.linesVertical(mCurrentToneScale);


        mSCanvasView = new GridMusicCanvas(getActivity(), builder.build(), mRlCanvasHolder);
        prepareSCanvas();
        mSCanvasView.setCurrentInstrument(InstrumentType.Piano);
        if (mSampleToLoad != null) {
            mSCanvasView.loadSample(getActivity(), (MelodySaver.GridMusicSample) mSampleToLoad);
            mSampleToLoad = null;
        }
        mRlCanvasHolder.addView(mImgvLine);
        mMediaPlayer = new MyMediaPlayer(getActivity().getApplicationContext(), mDuration);
        return mRootView;
    }

    private void prepareSCanvas() {
        mSCanvasView.setSCanvasInitializeListener(new SCanvasInitializeListener() {
            @Override
            public void onInitialized() {
                mSCanvasView.setZoomEnable(false);
                mSCanvasView.setEnableMultiTouch(false);
                mSCanvasView.setRemoveLongPressStroke(true);
                mSCanvasView.setStrokeLongClickSelectOption(false);
                mSCanvasView.setSObjectSelectListener(new SObjectSelectListener() {

                    @Override
                    public void onSObjectSelected(SObject arg0, boolean arg1) {
                        mSCanvasView.unSelectAllSObjects();
                    }
                });
                mSCanvasView.setSCanvasLongPressListener(new SCanvasLongPressListener() {

                    @Override
                    public void onLongPressed(float arg0, float arg1) {
                    }

                    @Override
                    public void onLongPressed() {
                    }
                });
            }
        });

    }

    private void findViews() {
        mRlCanvasHolder = (RelativeLayout) mRootView.findViewById(R.id.rlCanvasHolder);
        mImgvLine = new ImageView(getActivity());
        mImgvLine.setImageResource(R.drawable.line);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.setMargins(-20, 0, 0, 0);
        mImgvLine.setLayoutParams(params);
    }

    private void changeSurfaceParams(int toneScale, int timeDivide) {
        GridMusicCanvas.Params.Builder builder = new GridMusicCanvas.Params.Builder();
        builder.linesHorisontal(timeDivide);
        builder.linesVertical(toneScale);
        mRlCanvasHolder.removeAllViews();
        mSCanvasView = new GridMusicCanvas(getActivity(), builder.build(), mRlCanvasHolder);
        mRlCanvasHolder.addView(mImgvLine);
    }

    public void play() {
        stop();
        mSCanvasView.disableDraw();
        mMediaPlayer.setMusicMatrix(mSCanvasView.getTagMatrix());
        mMediaPlayer.startPlaying(mSCanvasView, false);
        mLineAnimator = ObjectAnimator.ofFloat(mImgvLine, "translationX", 0, mRlCanvasHolder.getWidth());
        mLineAnimator.setDuration(mDuration);
        mLineAnimator.setInterpolator(new LinearInterpolator());
        mLineAnimator.start();

    };

    public void stop() {
        mMediaPlayer.stopPlaying();
        if (mLineAnimator != null) {
            mLineAnimator.end();
        }
        mSCanvasView.enableDraw();

    };

    public void setInstrument(InstrumentType instrument) {
        mSCanvasView.setCurrentInstrument(instrument);
    };

    public boolean saveMusicTrack(String name) {
        List<MelodySaver.GridMusicSample> samples = MelodySaver.getSavedGridSamples(getActivity());
        for (MelodySaver.GridMusicSample sample : samples) {
            if (sample.getName().equals(name)) {
                return false;
            }
        }
        MelodySaver.saveGridMusicSample(getActivity().getApplicationContext(), name, mSCanvasView.getTagMatrix());
        return true;
    }

    public boolean setErase() {
        return mSCanvasView.setErase();
    }

    public void clean() {
        mSCanvasView.clean();
    }

    public void loadSample(Object sample) {
        if (mSCanvasView != null) {
            mSCanvasView.loadSample(getActivity(), (MelodySaver.GridMusicSample) sample);
        } else {
            mSampleToLoad = sample;
        }
    }

    public Serializable getDrawnSample() {
        return new MelodySaver.GridMusicSample("","", mSCanvasView.getTagMatrix());
    }
}
