package com.logtomobile.spen.grid_music;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.logtomobile.spen.InstrumentType;
import com.logtomobile.spen.MelodySaver;
import com.logtomobile.spen.MusicDrawer;
import com.samsung.samm.common.SObject;
import com.samsung.spensdk.SCanvasView;
import com.samsung.spensdk.applistener.SCanvasLongPressListener;
import com.samsung.spensdk.applistener.SObjectSelectListener;
import com.samsung.spensdk.applistener.SPenTouchListener;

/**
 * Created by gwiazda on 11.12.13.
 */
public class GridMusicCanvas extends SCanvasView implements MusicDrawer {
    private static final int LINE_THICKNESS = 8;
    private int mLinesHorisontal;
    private int mLinesVertical;
    private MyRectangleObject[][] mRectangleMatrix;

    private int mWidth;
    private int mHeight;
    private float mOffsetHorisontal;
    private float mOffsetVertical;

    private int mLastMatrixPositionX;
    private int mLastMatrixPositionY;

    private InstrumentType mCurrentInstrument;

    private Context mContext;

    private boolean mIsEraserSet;

    public GridMusicCanvas(Context context, Params params, RelativeLayout parentlayout) {
        super(context);

        mContext = context;

        mLinesHorisontal = params.getLinesHorisontal();
        mLinesVertical = params.getLinesVertical();

        mRectangleMatrix = new MyRectangleObject[mLinesHorisontal][mLinesVertical];

        for (int i = 0; i < mLinesHorisontal; i++) {
            for (int j = 0 ; j < mLinesVertical; j++) {
                mRectangleMatrix[i][j] = new MyRectangleObject();
            }
        }

        setOnTouchListener();

        MyBackgroundView canvasBackground= new MyBackgroundView(context, mLinesHorisontal, mLinesVertical, LINE_THICKNESS);
        canvasBackground.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        parentlayout.addView(canvasBackground);
        parentlayout.addView(this);

        final ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mWidth = getWidth();
                mHeight = getHeight();
                mOffsetHorisontal = ((float) mWidth/(float) mLinesHorisontal);
                mOffsetVertical = ((float) mHeight/(float) mLinesVertical);

                fillRectangleMatrix();
            }
        });
    }

    public void loadSample(final Context context, final MelodySaver.GridMusicSample sample) {
        for (int i = 0; i < mLinesHorisontal; i++) {
            for (int j = 0 ; j < mLinesVertical; j++) {
                MyRectangleObject rectangle = mRectangleMatrix[i][j];
                rectangle.setTag(null);
                rectangle.setColor(Color.TRANSPARENT);
            }
        }

        InstrumentType[][] instrumentMatrix = sample.getMusicMatrix();
        for (int i = 0; i < mLinesHorisontal; i++) {
            for (int j = 0; j < mLinesVertical; j++) {
                InstrumentType instrument = instrumentMatrix[i][j];
                if (instrument != null) {
                    mRectangleMatrix[i][j].setTag(instrument);
                    mRectangleMatrix[i][j].setColor(instrument.getInstrumentColor(context));
                }

            }
        }

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
            invalidate();
            }
        });
    }

    public void clean() {
        for (int i = 0; i < mLinesHorisontal; i++) {
            for (int j = 0 ; j < mLinesVertical; j++) {
                MyRectangleObject rectangle = mRectangleMatrix[i][j];
                rectangle.setTag(null);
                rectangle.setColor(Color.TRANSPARENT);
            }
        }
        invalidate();
    }

    public void enableDraw() {
        setOnTouchListener();
    }

    public void disableDraw() {
        this.setSPenTouchListener(null);
    }

    public void setCurrentInstrument(InstrumentType instrument) {
        mCurrentInstrument = instrument;
    }

    public boolean setErase() {
        mIsEraserSet = !mIsEraserSet;
        return mIsEraserSet;
    }

    public InstrumentType[][] getTagMatrix() {
        InstrumentType[][] tagMatrix = new InstrumentType[mLinesHorisontal][mLinesVertical];
        for (int i = 0; i < mLinesHorisontal; i++) {
            for (int j = 0; j < mLinesVertical; j++) {
                tagMatrix[i][j] = mRectangleMatrix[i][j].getTag();
            }
        }
        return tagMatrix;
    }

    private boolean onTouchEvent(View view, MotionEvent event) {
        if (event.getX() < mWidth && event.getX() > 0 && event.getY() < mHeight && event.getY() > 0) {
            int currentMatrixPositionX = (int) (event.getX() / mOffsetHorisontal);
            int currentMatrixPositionY = (int) (event.getY() / mOffsetVertical);

            if (event.getAction() == MotionEvent.ACTION_MOVE) {
                float offsetX = currentMatrixPositionX - mLastMatrixPositionX;
                float offsetY = currentMatrixPositionY - mLastMatrixPositionY;

                if (Math.abs(offsetX) > 1 || Math.abs(offsetY) > 1) {
                    int approximatedPositionX;
                    int approximatedPositionY;

                    if (Math.abs(offsetX) > Math.abs(offsetY)) {
                        if (offsetX > 0) {
                            for (int i =0; i < offsetX; i++) {
                                approximatedPositionX = mLastMatrixPositionX + i;
                                approximatedPositionY = mLastMatrixPositionY + (int) (offsetY * ((float) i) / offsetX);

                                MyRectangleObject rect = mRectangleMatrix [approximatedPositionX][approximatedPositionY];
                                if (mIsEraserSet) {
                                    if (rect.getTag() != null) {
                                        rect.setTag(null);
                                        rect.setColor(Color.TRANSPARENT);
                                        invalidate();
                                    }
                                } else if (!mCurrentInstrument.equals(rect.getTag())) {
                                    rect.setTag(mCurrentInstrument);
                                    rect.setColor(mCurrentInstrument.getInstrumentColor(mContext));
                                    invalidate();
                                }
                            }
                        } else {
                            for (int i =0; i > offsetX; i--) {
                                approximatedPositionX = mLastMatrixPositionX + i;
                                approximatedPositionY = mLastMatrixPositionY + (int) (offsetY * ((float) i) / offsetX);

                                MyRectangleObject rect = mRectangleMatrix [approximatedPositionX][approximatedPositionY];
                                if (mIsEraserSet) {
                                    if (rect.getTag() != null) {
                                        rect.setTag(null);
                                        rect.setColor(Color.TRANSPARENT);
                                        invalidate();
                                    }
                                } else if (!mCurrentInstrument.equals(rect.getTag())) {
                                    rect.setTag(mCurrentInstrument);
                                    rect.setColor(mCurrentInstrument.getInstrumentColor(mContext));
                                    invalidate();
                                }
                            }
                        }
                    } else {
                        if (offsetY > 0) {
                            for (int i =0; i < offsetY; i++) {
                                approximatedPositionX = mLastMatrixPositionX + (int) (offsetX * ((float) i) / offsetY);
                                approximatedPositionY = mLastMatrixPositionY + i;

                                MyRectangleObject rect = mRectangleMatrix [approximatedPositionX][approximatedPositionY];
                                if (mIsEraserSet) {
                                    if (rect.getTag() != null) {
                                        rect.setTag(null);
                                        rect.setColor(Color.TRANSPARENT);
                                        invalidate();
                                    }
                                } else if (!mCurrentInstrument.equals(rect.getTag())) {
                                    rect.setTag(mCurrentInstrument);
                                    rect.setColor(mCurrentInstrument.getInstrumentColor(mContext));
                                    invalidate();
                                }
                            }
                        } else {
                            for (int i =0; i > offsetY; i--) {
                                approximatedPositionX = mLastMatrixPositionX + (int) (offsetX * ((float) i) / offsetY);
                                approximatedPositionY = mLastMatrixPositionY + i;

                                MyRectangleObject rect = mRectangleMatrix [approximatedPositionX][approximatedPositionY];
                                if (mIsEraserSet) {
                                    if (rect.getTag() != null) {
                                        rect.setTag(null);
                                        rect.setColor(Color.TRANSPARENT);
                                        invalidate();
                                    }
                                } else if (!mCurrentInstrument.equals(rect.getTag())) {
                                    rect.setTag(mCurrentInstrument);
                                    rect.setColor(mCurrentInstrument.getInstrumentColor(mContext));
                                    invalidate();
                                }
                            }
                        }
                    }
                }

                MyRectangleObject rect = mRectangleMatrix[currentMatrixPositionX][currentMatrixPositionY];
                if (mIsEraserSet) {
                    if (rect.getTag() != null) {
                        rect.setTag(null);
                        rect.setColor(Color.TRANSPARENT);
                        invalidate();
                    }
                } else if (!mCurrentInstrument.equals(rect.getTag())) {
                    rect.setTag(mCurrentInstrument);
                    rect.setColor(mCurrentInstrument.getInstrumentColor(mContext));
                    invalidate();
                }
            }

            mLastMatrixPositionX = currentMatrixPositionX;
            mLastMatrixPositionY = currentMatrixPositionY;
        }

        return false;
    }

    @Override
    public void onDraw(Canvas canvas) {
        //super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);

        MyRectangleObject myRectangle;
        for (int i = 0; i < mLinesHorisontal; i++) {
            for (int j = 0; j < mLinesVertical; j++) {
                myRectangle = mRectangleMatrix[i][j];
                if (myRectangle.getColor() != Color.TRANSPARENT) {
                    paint.setColor(myRectangle.getColor());
                    canvas.drawRect(myRectangle.getRect(), paint);
                }
            }
        }
    }

    public void setOnTouchListener() {
        this.setSPenTouchListener(new SPenTouchListener() {
            @Override
            public boolean onTouchPen(View view, MotionEvent event) {
                return onTouchEvent(view, event);
            }

            @Override
            public boolean onTouchFinger(View view, MotionEvent event) {
                return onTouchEvent(view, event);
            }

            @Override
            public boolean onTouchPenEraser(View view, MotionEvent event) {
                return onTouchEvent(view, event);
            }

            @Override
            public void onTouchButtonDown(View view, MotionEvent motionEvent) {

            }

            @Override
            public void onTouchButtonUp(View view, MotionEvent motionEvent) {

            }
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }

    private void fillRectangleMatrix() {

        int offset = LINE_THICKNESS / 2;
        for (int i = 0; i < mLinesHorisontal; i++) {
            for (int j = 0 ; j < mLinesVertical; j++) {

                int startX = ((int) (mOffsetHorisontal * i)) + offset;
                int endX = (int) (mOffsetHorisontal * (i + 1)) - offset;
                int startY = ((int) (mOffsetVertical * j)) + offset;
                int endY = (int) (mOffsetVertical * (j + 1)) - offset;

                mRectangleMatrix[i][j].setRect(new Rect(startX, startY, endX, endY));
            }
        }
    }

    private class MyBackgroundView extends View {
        private int mLinesHorisontal;
        private int mLinesVertical;

        private int mHeight;
        private int mWidth;

        private int mLineThickness;

        public MyBackgroundView(Context context, int linesHorisontal, int linesVertical, int lineThickness) {
            super(context);
            mLinesHorisontal = linesHorisontal;
            mLinesVertical = linesVertical;
            mLineThickness = lineThickness;
        }

        @Override
        public void draw(Canvas canvas) {
            mHeight = this.getHeight();
            mWidth = this.getWidth();

            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(getContext().getResources().getColor(com.logtomobile.spen.R.color.activity_background_color));
            paint.setStrokeWidth(mLineThickness);

            float offsetHorisontal = ((float) mWidth/(float) mLinesHorisontal);
            float offsetVertical = ((float) mHeight/(float) mLinesVertical);

            Paint transparentPaint = new Paint();
            transparentPaint.setColor(Color.TRANSPARENT);
            transparentPaint.setStyle(Paint.Style.FILL);
            canvas.drawRect(new Rect(0, 0, mWidth, mHeight), transparentPaint);

            //drawEdges(canvas, mEdgeThickness);

            int positionX;
            for (int i = 0; i < mLinesHorisontal - 1; i++) {
                positionX = (int) (offsetHorisontal * (float)(i + 1));
                canvas.drawLine(positionX, 0, positionX, mHeight, paint);
            }

            int positionY;
            for (int i = 0; i < mLinesVertical - 1; i++) {
                positionY = (int) (offsetVertical * (float)(i + 1));
                canvas.drawLine(0, positionY, mWidth, positionY, paint);
            }
        }

        private void drawEdges(Canvas canvas, int edgeWidth) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(edgeWidth);

            int offset = (edgeWidth + 1) / 2;

            canvas.drawLine(offset, 0, offset, mHeight, paint);
            canvas.drawLine(mWidth - offset, 0, mWidth - offset, mHeight, paint);
            canvas.drawLine(0, offset, mWidth, offset, paint);
            canvas.drawLine(0, mHeight - offset, mWidth, mHeight - offset, paint);
        }
    }

    private class MyRectangleObject {
        private Rect mRect;
        private int mColor;
        private InstrumentType mTag;

        public MyRectangleObject() {
            mColor = Color.TRANSPARENT;
        }

        public void setRect(Rect rect) {
            mRect = rect;
        }

        public void setColor(int color) {
            mColor = color;
        }

        public int getColor() {
            return mColor;
        }

        public Rect getRect() {
            return mRect;
        }

        public void setTag(InstrumentType tag) {
            mTag = tag;
        }

        public InstrumentType getTag() {
            return mTag;
        }
    }

    public static class Params {
        Context mContext;
        private int mLinesHorisontal;
        private int mLinesVertical;

        private Params (Builder builder) {
            mLinesHorisontal = builder.mLinesHorisontal;
            mLinesVertical = builder.mLinesVertical;
        }

        public Context getContext() {
            return mContext;
        }

        public int getLinesHorisontal() {
            return mLinesHorisontal;
        }

        public int getLinesVertical() {
            return mLinesVertical;
        }

        public static class Builder {
            int mLinesHorisontal;
            int mLinesVertical;

            public Builder () {
            }

            public Builder linesHorisontal(int linesHorisontal) {
                mLinesHorisontal = linesHorisontal;
                return this;
            }

            public Builder linesVertical(int linesVertical) {
                mLinesVertical = linesVertical;
                return this;
            }

            public Params build() {
                return new Params (this);
            }
        }
    }

}