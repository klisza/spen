package com.logtomobile.spen;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.logtomobile.spen.grid_music.GridMusicFragment;
import com.logtomobile.spen.stave_music.StaveMusicFragment;

public class MainActivity extends Activity {
    public  static final String INTENT_EXTRA_CANVAS_MODE_STAVE = "canvas_mode_stave";
    private static final String SAVED_MUSIC_SAMPLE_TAG = "saved_music";
    private static final int RESULT_REQUEST_CODE = 1;
    private ImageView mImgvPlay;
    private ImageView mImgvStop;
    private ImageView mImgvGuitar;
    private ImageView mImgvTrumpet;
    private ImageView mImgvPiano;
    private ImageView mImgvDrums;
    private ImageView mImgvViolin;
    private TextView mTxtvErase;
    private ImageView mImgvEraseBar;
    private TextView mTxtvClean;

    private TextView mTxtvLoad;
    private TextView mTxtvSave;

    private FragmentType mCurrentFragment;
    private MusicFragmentInterface mMusicFragment;

    private boolean mIsStaveMode;

    private boolean mIsEraserSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        findViews();
        setListeners();
        setFonts();

        Bundle extras = getIntent().getExtras();
        if (extras.containsKey(INTENT_EXTRA_CANVAS_MODE_STAVE) && extras.getBoolean(INTENT_EXTRA_CANVAS_MODE_STAVE)) {
            mIsStaveMode = true;
            setFragment(FragmentType.StaveFragment);
        } else {
            mIsStaveMode = false;
            setFragment(FragmentType.GridFragment);
        }
        mImgvPiano.setSelected(true);

        if (savedInstanceState != null && savedInstanceState.containsKey(SAVED_MUSIC_SAMPLE_TAG)) {
            mMusicFragment.loadSample(savedInstanceState.getSerializable(SAVED_MUSIC_SAMPLE_TAG));
        }
        setTexts();

        initializeAudioFocusListener();
    }

    private void initializeAudioFocusListener() {
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        int result = am.requestAudioFocus(new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                if (mMusicFragment != null) {
                    mMusicFragment.stop();
                }
            }
        }, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
    }

    protected void onPause() {
        super.onPause();
        mMusicFragment.stop();
    }

    private void setTexts() {
        if (mIsStaveMode) {
            mTxtvLoad.setText("LOAD");
            mTxtvSave.setText("SAVE");
        } else {
            mTxtvLoad.setText("LOAD BOARD");
            mTxtvSave.setText("SAVE BOARD");
        }
    }

    private void setFonts() {
        FontSetter.setFont(this, mTxtvErase);
        FontSetter.setFont(this, mTxtvClean);
        FontSetter.setFont(this, mTxtvSave);
        FontSetter.setFont(this, mTxtvLoad);
    }

    private void setCurrentInstrument(InstrumentType instrument) {
        if (mIsEraserSet) {
            mIsEraserSet = mMusicFragment.setErase();
            mImgvEraseBar.setBackgroundColor(Color.TRANSPARENT);
        }
        mImgvPiano.setSelected(false);
        mImgvTrumpet.setSelected(false);
        mImgvDrums.setSelected(false);
        mImgvViolin.setSelected(false);
        mImgvGuitar.setSelected(false);
        switch (instrument) {
            case Piano:
                mImgvPiano.setSelected(true);
                break;
            case Trumpet:
                mImgvTrumpet.setSelected(true);
                break;
            case Drums:
                mImgvDrums.setSelected(true);
                break;
            case Violin:
                mImgvViolin.setSelected(true);
                break;
            case Guitar:
                mImgvGuitar.setSelected(true);
                break;
        }
        mMusicFragment.setInstrument(instrument);
    }

    public void setFragment(FragmentType type) {
        Fragment fragment;

        switch (type) {
           case GridFragment:
               GridMusicFragment gridFragment = new GridMusicFragment();
               mMusicFragment = gridFragment;
               fragment = gridFragment;
               break;
           case StaveFragment:
               StaveMusicFragment staveFragment = new StaveMusicFragment();
               mMusicFragment = staveFragment;
               fragment = staveFragment;
               break;
           default:
                GridMusicFragment grid = new GridMusicFragment();
                mMusicFragment = grid;
                fragment = grid;
                break;
        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.frameFragmentHolder, fragment);
        ft.commit();

        mCurrentFragment = type;

    }

    private void findViews() {

        mImgvPlay = (ImageView) findViewById(R.id.imgvPlay);
        mImgvStop = (ImageView) findViewById(R.id.imgvStop);
        mTxtvLoad = (TextView) findViewById(R.id.txtvLoad);
        mTxtvSave = (TextView) findViewById(R.id.txtvSave);
        mTxtvErase = (TextView) findViewById(R.id.txtvErase);
        mImgvEraseBar = (ImageView) findViewById(R.id.imgvEraseBar);
        mTxtvClean = (TextView) findViewById(R.id.txtvClean);


        mImgvPiano = (ImageView) findViewById(R.id.imgvPiano);
        mImgvGuitar = (ImageView) findViewById(R.id.imgvGuitar);
        mImgvTrumpet = (ImageView) findViewById(R.id.imgvTrumpet);
        mImgvDrums = (ImageView) findViewById(R.id.imgvDrums);
        mImgvViolin = (ImageView) findViewById(R.id.imgvViolin);
    }

    private void setListeners() {
        mImgvPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicFragment.play();
            }
        });

        mImgvStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicFragment.stop();
            }
        });

        mImgvGuitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentInstrument(InstrumentType.Guitar);
            }
        });

        mImgvTrumpet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentInstrument(InstrumentType.Trumpet);
            }
        });

        mImgvPiano.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentInstrument(InstrumentType.Piano);
            }
        });

        mImgvDrums.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentInstrument(InstrumentType.Drums);
            }
        });

        mImgvViolin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setCurrentInstrument(InstrumentType.Violin);
            }
        });


        /*mImgvNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mCurrentFragment) {
                    case GridFragment:
                        setFragment(FragmentType.StaveFragment);
                        break;
                    case StaveFragment:
                        setFragment(FragmentType.GridFragment);
                        break;
                }

            }
        });*/

        mTxtvErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMusicFragment.setErase()) {
                    mIsEraserSet = true;
                    mImgvEraseBar.setBackgroundColor(Color.RED);
                } else {
                    mIsEraserSet = false;
                    mImgvEraseBar.setBackgroundColor(Color.TRANSPARENT);
                };
            }
        });

        mTxtvClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicFragment.stop();
                mMusicFragment.clean();
            }
        });

        mTxtvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicFragment.stop();
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new SaveFileNameDialog();
                ft.add(newFragment, "save_dialog");
                ft.commit();
            }
        });

        mTxtvLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMusicFragment.stop();
                Intent intent = new Intent(MainActivity.this, MusicSamplesListActivity.class);
                intent.putExtra(INTENT_EXTRA_CANVAS_MODE_STAVE, mIsStaveMode);
                startActivityForResult(intent, RESULT_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            Bundle extras = data.getExtras();
            if (extras.containsKey("result")) {
                mMusicFragment.loadSample(extras.getSerializable("result"));
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putSerializable(SAVED_MUSIC_SAMPLE_TAG, mMusicFragment.getDrawnSample());
        super.onSaveInstanceState(savedInstanceState);
    }


    private enum FragmentType{
        GridFragment,
        StaveFragment,
    }

    private void showDialog(String title, String message) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(new MyAlertFragment(title, message), "alert");
        ft.commit();
    }

    private class MyAlertFragment extends DialogFragment {
        private String mTitle;
        private String mMessage;

        public MyAlertFragment(String title, String message) {
            super();
            mTitle = title;
            mMessage = message;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(mTitle);
            builder.setMessage(mMessage);
            builder.setPositiveButton("Ok", null);
            return builder.create();
        }
    }

    private class SaveFileNameDialog extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

            builder.setTitle("Saving music sample");

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            final EditText etName = new EditText(MainActivity.this);
            etName.setHint("Sample name");
            etName.setSingleLine();
            etName.setLayoutParams(params);
            RelativeLayout relative = new RelativeLayout(MainActivity.this);
            relative.setLayoutParams(params);

            relative.setPadding(20, 30, 30, 0);
            relative.addView(etName);

            builder.setView(relative);
            builder.setPositiveButton("Save", null);

            builder.setNegativeButton("Cancel", null);
            final AlertDialog alertDialog = builder.create();
            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    b.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name = etName.getText().toString().trim();
                            if (name.length() > 0) {
                                if (!mMusicFragment.saveMusicTrack(name)) {
                                    showDialog("Error", "This name is already used");
                                } else {
                                    dismiss();
                                }
                            } else {
                                showDialog("Error", "You have to enter name of the sample");
                            }
                        }
                    });
                }
            });

            return alertDialog;
        }
    }
}
