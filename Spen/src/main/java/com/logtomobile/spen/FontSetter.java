package com.logtomobile.spen;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Created by gwiazda on 29.12.13.
 */
public class FontSetter {
        public static Typeface sTypeFace;

        public static void setFont(Context context, TextView txtvName) {
            if (sTypeFace == null) {
                sTypeFace = Typeface.createFromAsset(context.getAssets(), "bebas_neue.otf");
            }

            txtvName.setTypeface(sTypeFace);

        }
}
