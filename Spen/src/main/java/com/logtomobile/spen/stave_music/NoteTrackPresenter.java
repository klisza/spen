package com.logtomobile.spen.stave_music;

import com.logtomobile.spen.InstrumentType;

/**
 * Created by gwiazda on 19.12.13.
 */
public interface NoteTrackPresenter {
    public void showCurrentNote(InstrumentType instrument, int position);
    public void showNoneNote(InstrumentType instrument);
    public void enableDraw();
}
