package com.logtomobile.spen.stave_music;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Handler;
import android.os.Looper;

import com.google.common.collect.Lists;
import com.logtomobile.spen.InstrumentType;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * Created by gwiazda on 16.12.13.
 */
public class MyNotePlayer {
    private static final float ONE_NOTE_DURATION = 2000f;
    private static final Note emptyNote = new Note(0, Note.ChromaticScale.four);
    private static double mToneFrequencyMultiplier = Math.pow(2, 1d/7d);

    private volatile List<Thread> mMusicThreads = Lists.newLinkedList();

    private HashMap<InstrumentType, Integer> mPoolSoundIdMap = new HashMap<InstrumentType, Integer>();
    private HashSet<Integer> mCurrentStreamsSet = new HashSet<Integer>();
    private List<Note.NoteTrack> mNoteTracks;

    private Context mContext;
    private SoundPool mSoundPool;

    private Map<Note.ChromaticScale, Integer> mPianoSoundsMap = new HashMap<Note.ChromaticScale, Integer>();

    private final Object mLockObj = new Object();

    private final Object mLockObjectStreamsSet = new Object();

    public MyNotePlayer(Context context) {
        mContext = context.getApplicationContext();
        mSoundPool = new SoundPool(40, AudioManager.STREAM_MUSIC, 100);

        loadSounds();
    }

    private void loadSounds() {
        for (InstrumentType instrument : InstrumentType.values()) {
            mPoolSoundIdMap.put(instrument, mSoundPool.load(mContext, instrument.getResourceID(), 1));
        }
    }

    public void setNoteTracks(List<Note.NoteTrack> tracks) {
        mNoteTracks = tracks;
    }

    public void stopPlaying() {
        synchronized (mLockObj) {
            if (!mMusicThreads.isEmpty()) {
                for (Thread musicThread : mMusicThreads) {
                    musicThread.interrupt();
                }
            }
            mMusicThreads.clear();

            synchronized (mLockObjectStreamsSet) {
                for (Integer streamId : mCurrentStreamsSet) {
                    mSoundPool.stop(streamId);
                }
                mCurrentStreamsSet.clear();
            }

        }
    }

    public void startPlaying(NoteTrackPresenter presenter) {
        stopPlaying();
        if (mNoteTracks != null && !mNoteTracks.isEmpty()) {
            for (Note.NoteTrack noteTrack : mNoteTracks) {
                playNoteTrack(noteTrack, presenter);
            }
        }
    }

    private void playNoteTrack (final Note.NoteTrack track, final NoteTrackPresenter presenter) {
        final InstrumentType type = track.getInstrumentType();
        final Thread musicThread = new Thread(new Runnable() {
            @Override
            public void run() {

                List<Note> notes = track.getNotes();

                for (int i = 0; i < notes.size(); i++) {
                    final int finalIter = i;
                    final Note note = notes.get(i);
                    if (!note.equals(Note.INVALID_NOTE)) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                presenter.showCurrentNote(type, finalIter);
                                playSound(type, countNoteDuration(note.getDuration()), note.getTone());
                            }
                        }).start();
                        try {
                            Thread.sleep(countNoteDuration(note.getDuration()));
                        } catch (InterruptedException ie) {
                            presenter.showNoneNote(track.getInstrumentType());
                            return;
                        }
                    } else {
                        presenter.showNoneNote(type);
                        try {
                            Thread.sleep(countNoteDuration(emptyNote.getDuration()));
                        } catch (InterruptedException ie) {
                            presenter.showNoneNote(track.getInstrumentType());
                            return;
                        }
                    }
                }
                presenter.showNoneNote(track.getInstrumentType());

                if (mMusicThreads.contains(Thread.currentThread())) {
                    synchronized (mLockObj) {
                        mMusicThreads.remove(Thread.currentThread());
                    }
                }
                if (mMusicThreads.isEmpty()) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            presenter.enableDraw();
                        }
                    });
                }
            }
        });
        synchronized (mLockObj) {
            mMusicThreads.add(musicThread);
        }
        musicThread.start();
    }



    private void playSound(InstrumentType instrumentType, final long duration, int level) {
        float countedRate = countPlayRate(level);
        final Integer currentStreamID = mSoundPool.play(mPoolSoundIdMap.get(instrumentType), 1f, 1f, 1, 0, countedRate);
        synchronized (mLockObjectStreamsSet) {
            mCurrentStreamsSet.add(currentStreamID);
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(duration);
                    synchronized (mLockObjectStreamsSet) {
                        mCurrentStreamsSet.remove(currentStreamID);
                    }
                    mSoundPool.stop(currentStreamID);
                } catch (InterruptedException ie) {
                    synchronized (mLockObjectStreamsSet) {
                        mCurrentStreamsSet.remove(currentStreamID);
                    }
                    mSoundPool.stop(currentStreamID);
                }

            }
        }).start();
    }

    private void playNoteTrack2 (final Note.NoteTrack track, final NoteTrackPresenter presenter) {
        final InstrumentType type = track.getInstrumentType();
        Thread musicThread = new Thread(new Runnable() {
            @Override
            public void run() {

                List<Note> notes = track.getNotes();

                for (int i = 0; i < notes.size(); i++) {
                    final int finalIter = i;
                    final Note note = notes.get(i);
                    if (!note.equals(Note.INVALID_NOTE)) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                presenter.showCurrentNote(type, finalIter);
                                playSound2(note);
                            }
                        }).start();
                        try {
                            Thread.sleep(countNoteDuration(note.getDuration()));
                        } catch (InterruptedException ie) {
                            presenter.showNoneNote(track.getInstrumentType());
                            break;
                        }
                    }
                }
                presenter.showNoneNote(track.getInstrumentType());

            }
        });
        mMusicThreads.add(musicThread);
        musicThread.start();
    }

    private void playSound2(Note note) {
        float countedRate = countPlayRate(note.getTone());
        final Integer currentStreamID = mSoundPool.play(mPianoSoundsMap.get(note.getChromaticScale()), 1f, 1f, 1, 0, countedRate);
        /*mCurrentStreamsSet.add(currentStreamID);
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(duration);
                    mCurrentStreamsSet.remove(currentStreamID);
                } catch (InterruptedException ie) {
                    mCurrentStreamsSet.remove(currentStreamID);
                }

            }
        }).start();*/
    }


    private long countNoteDuration(float chromaticScale) {
        return (long) (ONE_NOTE_DURATION * chromaticScale);
    }

    private float countPlayRate(int level) {
        return ((float) Math.pow(mToneFrequencyMultiplier, (double) level));
    }



}
