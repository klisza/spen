package com.logtomobile.spen.stave_music;

import com.google.common.collect.Lists;
import com.logtomobile.spen.InstrumentType;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gwiazda on 18.12.13.
 */
public class Note implements Serializable{
    private static final long serialVersionUID = 754598675087625L;
    public static final Note INVALID_NOTE = new Note();

    public static HashMap<ChromaticScale, Float> sDurationMap;

    static {
        sDurationMap = new HashMap<ChromaticScale, Float>();
        sDurationMap.put(ChromaticScale.one, 1f);
        sDurationMap.put(ChromaticScale.half, 1f / 2f);
        sDurationMap.put(ChromaticScale.four, 1f / 4f);
        sDurationMap.put(ChromaticScale.eight, 1f / 8f);
        sDurationMap.put(ChromaticScale.sixteen, 1f / 16f);
    }

    private int mTone;
    private ChromaticScale mChromaticScale;

    private Note() {
        mTone = -100;
        mChromaticScale = ChromaticScale.one;
    }

    public Note(int tone, ChromaticScale chromaticScale) {
        if (tone > 7) {
            mTone = 7;
        } else if (tone < -7) {
            mTone = -7;
        } else {
            mTone = tone;
        }
        mChromaticScale = chromaticScale;
    }

    public float getDuration() {
        return sDurationMap.get(mChromaticScale);
    }

    public int getTone() {
        return mTone;
    }

    public ChromaticScale getChromaticScale() {
        return mChromaticScale;
    }

    public static enum ChromaticScale {
        one,
        half,
        four,
        eight,
        sixteen
    }

    public static class NoteTrack implements Serializable{
        private static final long serialVersionUID = 47878639275428L;
        private InstrumentType mInstrumentType;
        private List<Note> mNotes;

        public NoteTrack (InstrumentType type, int length) {
            mInstrumentType = type;
            mNotes = Lists.newLinkedList();
            for (int i = 0; i < length; i++) {
                mNotes.add(INVALID_NOTE);
            }
        }

        public void setNote(int position, Note note) {
            if (position < mNotes.size()) {
                mNotes.set(position, note);
            }
        }

        public void setNotesList(List<Note> notes) {
            mNotes = notes;
        }

        public InstrumentType getInstrumentType() {
            return mInstrumentType;
        }

        public List<Note> getNotes() {
            return mNotes;
        }
    }

    @Override
    public boolean equals(Object o) {
        Note note = (Note) o;
        return (this.mChromaticScale.equals(note.mChromaticScale) && this.mTone == note.mTone);
    }
}
