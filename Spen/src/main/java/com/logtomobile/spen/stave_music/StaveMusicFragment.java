package com.logtomobile.spen.stave_music;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.logtomobile.spen.InstrumentType;
import com.logtomobile.spen.MelodySaver;
import com.logtomobile.spen.MusicFragmentInterface;
import com.logtomobile.spen.R;
import com.logtomobile.spen.nn.training.NetworkTrainer;
import com.logtomobile.spen.nn.training.OnProgressChangedListener;
import com.samsung.samm.common.SObject;
import com.samsung.spensdk.SCanvasView;
import com.samsung.spensdk.applistener.SCanvasInitializeListener;
import com.samsung.spensdk.applistener.SCanvasLongPressListener;
import com.samsung.spensdk.applistener.SObjectSelectListener;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

/**
 * Created by gwiazda on 13.12.13.
 */
public class StaveMusicFragment extends Fragment implements MusicFragmentInterface {
    private static final String NEURAL_NETWORK_FILENAME = Environment.getExternalStorageDirectory() + "/neural_network";

    private View mRootView;
    private RelativeLayout mRlCanvasHolder;
    private LinearLayout mLlNotesHolder;
    private ProgressBar mPbTrain;

    private TextView mTxtvNote1;
    private TextView mTxtvNote2;
    private TextView mTxtvNote3;
    private TextView mTxtvNote4;
    private TextView mTxtvNote5;
    private TextView mTxtvSave;

    private OnProgressChangedListener mTrainListener = new OnProgressChangedListener() {
        @Override
        public void onProgressChanged(final int progress, double error) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (progress < NetworkTrainer.MAX_EPOCH_NUMBER){
                        mPbTrain.setVisibility(View.VISIBLE);
                        mPbTrain.setProgress(progress);
                    } else {
                        mPbTrain.setVisibility(View.GONE);
                    }
                }
            });
        }
    };

    private StaveMusicCanvas mSCanvasView;
    private MyNotePlayer mNotePlayer;

    private Object mSampleToLoad;

    private HashMap<Character, double[]> mCharsMap = new HashMap<Character, double[]>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_stave_music, container,  false);

        findViews();
        setListeners();
        initCharsMap();

        mSCanvasView = new StaveMusicCanvas(getActivity(), mRlCanvasHolder);
        prepareSCanvas();
        mSCanvasView.setCurrentInstrument(InstrumentType.Piano);
        if (mSampleToLoad != null) {
            mSCanvasView.loadSample((MelodySaver.StaveMusicSample) mSampleToLoad);
            mSampleToLoad = null;
        }

        mNotePlayer = new MyNotePlayer(getActivity().getApplicationContext());
        return mRootView;
    }

    private void prepareSCanvas() {
        mSCanvasView.setSCanvasInitializeListener(new SCanvasInitializeListener() {
            @Override
            public void onInitialized() {
                mSCanvasView.setZoomEnable(false);
                mSCanvasView.setEnableMultiTouch(false);
                mSCanvasView.setRemoveLongPressStroke(true);
                mSCanvasView.setStrokeLongClickSelectOption(false);
                mSCanvasView.setSObjectSelectListener(new SObjectSelectListener() {

                    @Override
                    public void onSObjectSelected(SObject arg0, boolean arg1) {
                        mSCanvasView.unSelectAllSObjects();
                    }
                });
                mSCanvasView.setSCanvasLongPressListener(new SCanvasLongPressListener() {

                    @Override
                    public void onLongPressed(float arg0, float arg1) {
                    }

                    @Override
                    public void onLongPressed() {
                    }
                });
            }
        });

    }

    private void initCharsMap() {
        mCharsMap.put('0', new double[] {0.0, 0.0, 0.0, 0.0, 0.0});
        mCharsMap.put('1', new double[] {1.0, 0.0, 0.0, 0.0, 0.0});
        mCharsMap.put('2', new double[] {0.0, 1.0, 0.0, 0.0, 0.0});
        mCharsMap.put('3', new double[] {0.0, 0.0, 1.0, 0.0, 0.0});
        mCharsMap.put('4', new double[] {0.0, 0.0, 0.0, 1.0, 0.0});
        mCharsMap.put('5', new double[] {0.0, 0.0, 0.0, 0.0, 1.0});
    }

    private void findViews() {
        mRlCanvasHolder = (RelativeLayout) mRootView.findViewById(R.id.rlCanvasHolder);

        mLlNotesHolder = (LinearLayout) mRootView.findViewById(R.id.llNotesHolder);
        mPbTrain = (ProgressBar) mRootView.findViewById(R.id.pbTrain);
        mPbTrain.setMax(NetworkTrainer.MAX_EPOCH_NUMBER);
        mTxtvNote1 = (TextView) mRootView.findViewById(R.id.txtvNote1);
        mTxtvNote2 = (TextView) mRootView.findViewById(R.id.txtvNote2);
        mTxtvNote3 = (TextView) mRootView.findViewById(R.id.txtvNote3);
        mTxtvNote4 = (TextView) mRootView.findViewById(R.id.txtvNote4);
        mTxtvNote5 = (TextView) mRootView.findViewById(R.id.txtvNote5);
        mTxtvSave = (TextView) mRootView.findViewById(R.id.txtvSave);
    }

    private void setListeners() {
       mTxtvNote1.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mSCanvasView.train(mCharsMap.get('1'), mTrainListener);
           }
       });
        mTxtvNote2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSCanvasView.train(mCharsMap.get('2'), mTrainListener);
            }
        });
        mTxtvNote3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSCanvasView.train(mCharsMap.get('3'), mTrainListener);
            }
        });
        mTxtvNote4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSCanvasView.train(mCharsMap.get('4'), mTrainListener);
            }
        });
        mTxtvNote5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSCanvasView.train(mCharsMap.get('5'), mTrainListener);
            }
        });
        mTxtvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSCanvasView.saveNeuralNetwork(NEURAL_NETWORK_FILENAME);
            }
        });
    }


    public void play() {
        mNotePlayer.setNoteTracks(mSCanvasView.getNoteTracks());
        mNotePlayer.startPlaying(mSCanvasView);
        mSCanvasView.disableDraw();

    };
    public void stop() {
        mNotePlayer.stopPlaying();
        mSCanvasView.enableDraw();
    };
    public void setInstrument(InstrumentType instrument) {
        mSCanvasView.setCurrentInstrument(instrument);
    };
    public boolean saveMusicTrack(String name) {
        List<MelodySaver.StaveMusicSample> samples = MelodySaver.getSavedStaveSamples(getActivity());
        for (MelodySaver.StaveMusicSample sample : samples) {
            if (sample.getName().equals(name)) {
                return false;
            }
        }
        MelodySaver.saveStaveMusicSample(getActivity().getApplicationContext(), name, mSCanvasView.getNoteTracks());
        return true;
    }
    public boolean setErase() {
        return mSCanvasView.setErase();
    }
    public void clean() {
        mSCanvasView.clean();
    }
    public void loadSample(Object sample) {
        if (mSCanvasView != null) {
            mSCanvasView.loadSample((MelodySaver.StaveMusicSample) sample);
        } else {
            mSampleToLoad = sample;
        }
    }

    public Serializable getDrawnSample() {
        return new MelodySaver.StaveMusicSample("","", mSCanvasView.getNoteTracks());
    }
}
