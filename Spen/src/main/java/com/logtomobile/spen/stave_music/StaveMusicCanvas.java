package com.logtomobile.spen.stave_music;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.logtomobile.spen.Constants;
import com.logtomobile.spen.InstrumentType;
import com.logtomobile.spen.MelodySaver;
import com.logtomobile.spen.SecondStepRecognizer;
import com.logtomobile.spen.nn.Serializer;
import com.logtomobile.spen.nn.CharacterRecognizer;
import com.logtomobile.spen.nn.DownSampler;
import com.logtomobile.spen.nn.training.LearningSample;
import com.logtomobile.spen.nn.training.NetworkTrainer;
import com.logtomobile.spen.nn.training.OnProgressChangedListener;
import com.samsung.samm.common.SObject;
import com.samsung.spensdk.SCanvasView;
import com.samsung.spensdk.applistener.SCanvasLongPressListener;
import com.samsung.spensdk.applistener.SObjectSelectListener;
import com.samsung.spensdk.applistener.SPenTouchListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by gwiazda on 13.12.13.
 */
public class StaveMusicCanvas extends SCanvasView implements NoteTrackPresenter {
    private static final boolean SAVE_DRAWN_IMAGES = false;
    private static final int TOP_PADDING = 130;
    private static final int SCANVAS_LEFT_PADDING = 250;
    private static final int SCANVAS_BOTTOM_PADDING = 100;
    private static final long THREAD_DELAY = 1000;
    private static final float NOTE_HEIGHT_RATIO = 6f;
    private static final float NOTE_WIDTH_RATIO = 5f;
    private static final float NOTE_CENTER_RATIO = 1f / 6f;
    private static final int STAVE_MAXIMUM_SCALE = 7;
    private static final float MAXIMUM_DISTANCE_FACTOR = 0.3f;

    private Context mContext;
    private int mLinesHorisontal;
    private int mLinesVertical;
    private MyNoteRect[][] mRectangleMatrix;

    private CharacterRecognizer mRecognizer;

    private Thread mCurrentDrawEndingThread;

    private int mCurrentColor;
    private int mWidth;
    private int mHeight;
    private int mActiveAreaHeight;
    private float mOffsetHorisontal;
    private float mOffsetVertical;
    private int mNoteWidth;
    private int mNoteHeight;

    private float mCurrentMoveStartX;
    private float mCurrentMoveEndX;
    private float mCurrentMoveStartY;
    private float mCurrentMoveEndY;

    private int mDrawingAreaStartX = -1;
    private int mDrawingAreaEndX = -1;
    private int mDrawingAreaStartY = -1;
    private int mDrawingAreaEndY = -1;

    private double[] mLastDrawnNote;


    private HashMap<Character, Note.ChromaticScale> mChromaticScaleMap;
    private HashMap<Note.ChromaticScale, Bitmap> mNotesBitmapsMap;

    private HashMap<InstrumentType, Note.NoteTrack> mTrackMap = new HashMap<InstrumentType, Note.NoteTrack>();
    private HashMap<InstrumentType, Integer> mCurrentPlayedNotesMap = new HashMap<InstrumentType, Integer>();

    private Note.NoteTrack mCurrentNoteTrack;
    private InstrumentType mCurrentInstrument;

    private boolean mIsEraserSet;
    private boolean mIsDrawingDisabled;

    private View mNoteOverlay;

    private int mBackgroundColor;


    public StaveMusicCanvas(Context context, RelativeLayout parentlayout) {
        super(context);
        setDrawingCacheEnabled(true);
        mContext = context.getApplicationContext();
        mRecognizer = CharacterRecognizer.getInstance(context);

        //loadLearningSequence();

        mLinesHorisontal = 12;
        mLinesVertical = 15;

        mRectangleMatrix = new MyNoteRect[mLinesHorisontal][mLinesVertical];

        mCurrentColor = Color.BLACK;

        setOnTouchListener();

        prepareChromaticScaleMap();
        MyBackgroundView canvasBackground= new MyBackgroundView(context, mLinesHorisontal, mLinesVertical, 5, 1);
        mNoteOverlay = new View(mContext);
        mBackgroundColor = context.getResources().getColor(com.logtomobile.spen.R.color.activity_background_color);
        mNoteOverlay.setBackgroundColor(mBackgroundColor);
        //mNoteOverlay.setVisibility(View.GONE);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(0, 0);
        mNoteOverlay.setLayoutParams(params);

        params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(SCANVAS_LEFT_PADDING, 0, 0, SCANVAS_BOTTOM_PADDING);
        this.setLayoutParams(params);
        parentlayout.addView(this);
        parentlayout.addView(mNoteOverlay);
        parentlayout.addView(canvasBackground);

        initializeNoteTracks();
        clearCurrentNotesMap();
        final ViewTreeObserver vto = getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mWidth = getWidth();
                mHeight = getHeight();
                mActiveAreaHeight = mHeight - TOP_PADDING;
                mOffsetHorisontal = ((float) mWidth / (float) mLinesHorisontal);
                mOffsetVertical = ((float) mActiveAreaHeight / (float) mLinesVertical);

                for (int i = 0; i < mLinesHorisontal; i++) {
                    for (int j = 0; j < mLinesVertical; j++) {

                        int startX = (int) ((mOffsetHorisontal * ((float) i)) + (mOffsetHorisontal / 6f));
                        int startY = TOP_PADDING + (int) ((mOffsetVertical * ((float) j)) + (mOffsetVertical / 2f) - mNoteHeight + (((float) mNoteHeight) * NOTE_CENTER_RATIO));

                        mRectangleMatrix[i][j] = new MyNoteRect(startX, startY);
                    }
                }
                prepareScaledNotes();
            }
        });
    }


    public void loadSample(MelodySaver.StaveMusicSample sample) {
        mTrackMap = new HashMap<InstrumentType, Note.NoteTrack>();
        for (Note.NoteTrack track : sample.getNoteTrack()) {
            mTrackMap.put(track.getInstrumentType(), track);
        }
        mCurrentNoteTrack = mTrackMap.get(mCurrentInstrument);
        invalidate();
    }

    public void clean() {
        mTrackMap = new HashMap<InstrumentType, Note.NoteTrack>();
        initializeNoteTracks();
        mCurrentNoteTrack = mTrackMap.get(mCurrentInstrument);
        invalidate();

    }

    public boolean setErase() {
        mIsEraserSet = !mIsEraserSet;
        if (!mIsEraserSet) {
            try {
                clearScreen();
            } catch (NullPointerException npe) {
                //nothing to do
            }
            destroyDrawingCache();
            invalidate();
        }
        return mIsEraserSet;
    }

    private void initializeNoteTracks() {
        for (InstrumentType instrument : InstrumentType.values()) {
            mTrackMap.put(instrument, new Note.NoteTrack(instrument, mLinesHorisontal));
        }
    }

    private void clearCurrentNotesMap() {
        for (InstrumentType instrument : InstrumentType.values()) {
            mCurrentPlayedNotesMap.put(instrument, -1);
        }
    }

    private void loadLearningSequence() {
        try {
            Serializer serializer = Serializer.getInstance();
            LinkedList<LearningSample> seq = (LinkedList<LearningSample>)serializer.load(mContext, Constants.LEARNING_SEQUENCE_FILENAME);
            mRecognizer.setLearningSequence(seq);
        } catch (Exception e) {
            Log.d("doszlo", "wyjątek w StaveMusicCanvas w onCreate");
        }
    }

    private void prepareChromaticScaleMap() {
        mChromaticScaleMap = new HashMap<Character, Note.ChromaticScale>();
        mChromaticScaleMap.put('1', Note.ChromaticScale.half);
        mChromaticScaleMap.put('2', Note.ChromaticScale.eight);
        mChromaticScaleMap.put('3', Note.ChromaticScale.sixteen);
    }

    private void prepareScaledNotes() {
        mNoteWidth = (int) (mOffsetVertical * NOTE_WIDTH_RATIO);
        mNoteHeight = (int) (mOffsetVertical * NOTE_HEIGHT_RATIO);
        Resources res = mContext.getResources();

        mNotesBitmapsMap = new HashMap<Note.ChromaticScale, Bitmap>();
        mNotesBitmapsMap.put(Note.ChromaticScale.one, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, com.logtomobile.spen.R.drawable.note_one), mNoteWidth, mNoteHeight, false));
        mNotesBitmapsMap.put(Note.ChromaticScale.half, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, com.logtomobile.spen.R.drawable.note_half), mNoteWidth, mNoteHeight, false));
        mNotesBitmapsMap.put(Note.ChromaticScale.four, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, com.logtomobile.spen.R.drawable.note_four), mNoteWidth, mNoteHeight, false));
        mNotesBitmapsMap.put(Note.ChromaticScale.eight, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, com.logtomobile.spen.R.drawable.note_eight), mNoteWidth, mNoteHeight, false));
        mNotesBitmapsMap.put(Note.ChromaticScale.sixteen, Bitmap.createScaledBitmap(BitmapFactory.decodeResource(res, com.logtomobile.spen.R.drawable.note_sixteen), mNoteWidth, mNoteHeight, false));

    }

    public List<Note.NoteTrack> getNoteTracks() {
        List<Note.NoteTrack> tracks = new LinkedList<Note.NoteTrack>();
        for (Note.NoteTrack noteTrack : mTrackMap.values()) {
            tracks.add(noteTrack);
        }
        return tracks;
    }

    public void showCurrentNote(final InstrumentType instrument, final int position) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mCurrentPlayedNotesMap.put(instrument, position);
                invalidate();
            }
        });
    }

    public void showNoneNote(final InstrumentType instrument) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                mCurrentPlayedNotesMap.put(instrument, -1);
                invalidate();
            }
        });
    }

    public void setCurrentInstrument(InstrumentType instrument) {
        mCurrentInstrument = instrument;
        mCurrentNoteTrack = mTrackMap.get(instrument);
        invalidate();
    }

    private boolean onTouchEvent(View view, MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {

            if (mCurrentDrawEndingThread != null) {
                stopDrawignEndingThread();

                int maximumDistance = (int) (MAXIMUM_DISTANCE_FACTOR * mOffsetHorisontal);
                if ((mCurrentMoveStartX - event.getX()) > maximumDistance || (event.getX() - mCurrentMoveEndX) > maximumDistance) {
                    recognizaAndSetNote(false);
                    mCurrentMoveStartX = event.getX();
                    mCurrentMoveEndX = event.getX() + 1;
                    mCurrentMoveStartY = event.getY();
                    mCurrentMoveEndY = event.getY() + 1;
                }
            } else {
                mCurrentMoveStartX = event.getX();
                mCurrentMoveEndX = event.getX() + 1;
                mCurrentMoveStartY = event.getY();
                mCurrentMoveEndY = event.getY() + 1;
            }

        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            if (event.getX() < mWidth && event.getX() > 0 && event.getY() < mHeight && event.getY() > 0) {
                if (event.getX() < mCurrentMoveStartX) {
                    mCurrentMoveStartX = event.getX();
                } else if (event.getX() > mCurrentMoveEndX) {
                    mCurrentMoveEndX = event.getX();
                }

                if (event.getY() < mCurrentMoveStartY) {
                    mCurrentMoveStartY = event.getY();
                } else if (event.getY() > mCurrentMoveEndY) {
                    mCurrentMoveEndY = event.getY();
                }

                int areaStartX = (int) (mCurrentMoveStartX / mOffsetHorisontal);
                int areaEndX = (int) (mCurrentMoveEndX / mOffsetHorisontal);
                int areaStartY = (int) ((mCurrentMoveStartY - TOP_PADDING) / mOffsetVertical) - 1;
                int areaEndY = (int) ((mCurrentMoveEndY - TOP_PADDING) / mOffsetVertical) + (int) (mNoteHeight / mOffsetVertical) - 1;
                if (areaStartY < 0 ) {
                    areaStartY = 0;
                }
                if (areaEndY < 0) {
                    areaEndY = 0;
                }

                if (mDrawingAreaStartX != areaStartX || mDrawingAreaEndX != areaEndX || mDrawingAreaStartY != areaStartY || mDrawingAreaEndY != areaEndY) {
                    mDrawingAreaStartX = areaStartX;
                    mDrawingAreaEndX = areaEndX;
                    mDrawingAreaStartY = areaStartY;
                    mDrawingAreaEndY = areaEndY;

                    invalidate();
                }
            }

        } else if (event.getAction() == MotionEvent.ACTION_UP) {
            setDrawEndingThread();
        }

        return false;
    }

    private void stopDrawignEndingThread() {
        if (mCurrentDrawEndingThread != null) {
            mCurrentDrawEndingThread.interrupt();
        }
        mCurrentDrawEndingThread = null;
    }

    private Bitmap overlay(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(bmp1, new Matrix(), null);
        canvas.drawBitmap(bmp2, new Matrix(), null);
        return bmOverlay;
    }

    private void recognizaAndSetNote(final boolean showAnimations) {
        int bitmapWidth = (int) (mCurrentMoveEndX - mCurrentMoveStartX) + 10;
        int bitmapHeight = (int) (mCurrentMoveEndY - mCurrentMoveStartY) + 10;

        int startX = (int) mCurrentMoveStartX - 5;
        int startY = (int) mCurrentMoveStartY - 5;
        if (startX < 0) {
            startX = 0;
        }
        if (startY < 0) {
            startY = 0;
        }
        if (startX + bitmapWidth >= mWidth) {
            bitmapWidth = mWidth - startX;
        }
        if (startY + bitmapHeight >= mHeight) {
            bitmapHeight = mHeight - startY;
        }

        Bitmap note = null;
        try {
            note = Bitmap.createBitmap(getDrawingCache(), startX, startY, bitmapWidth, bitmapHeight);
        } catch (NullPointerException npe) {
            try {
                clearScreen();
            } catch (NullPointerException np) {
                //nothing to do
            }
            destroyDrawingCache();
            mDrawingAreaStartX = -1;
            mDrawingAreaEndX = -1;
            mDrawingAreaStartY = -1;
            mDrawingAreaEndY = -1;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    invalidate();
                }
            });
            return;
        } catch (RuntimeException re) {
            try {
                clearScreen();
            } catch (NullPointerException np) {
                //nothing to do
            }
            destroyDrawingCache();
            mDrawingAreaStartX = -1;
            mDrawingAreaEndX = -1;
            mDrawingAreaStartY = -1;
            mDrawingAreaEndY = -1;
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    invalidate();
                }
            });
            return;
        }
        final Bitmap noteBitmap = note;
        try {
            clearScreen();
        } catch (NullPointerException npe) {
            //nothing to do
        }
        destroyDrawingCache();

        mLastDrawnNote = DownSampler.processImage(noteBitmap, getDropperColor());

        Note.ChromaticScale chromaticScale = null;
        if (bitmapHeight < mOffsetVertical * 4) {
            if (bitmapWidth < mOffsetVertical * 6) {
                chromaticScale = Note.ChromaticScale.one;
            }
        } else {
            chromaticScale = mChromaticScaleMap.get(mRecognizer.recognize(mLastDrawnNote));
            if (chromaticScale == Note.ChromaticScale.half) {
                if (SecondStepRecognizer.isHalfNote(mLastDrawnNote, Constants.DOWNSAMPLE_WIDTH, Constants.DOWNSAMPLE_HEIGHT)) {
                    chromaticScale = Note.ChromaticScale.half;
                } else {
                    chromaticScale = Note.ChromaticScale.four;
                }
            }
        }

        int currentMatrixPositionX = -1;
        int currentMatrixPositionY = -1;
        if (chromaticScale != null) {
            float positionX = mCurrentMoveStartX + ((float) noteBitmap.getWidth()) / 3f;
            float positionY;

            if (chromaticScale.equals(Note.ChromaticScale.one)) {
                positionY = mCurrentMoveEndY - TOP_PADDING - (((float) noteBitmap.getHeight()) / 2f) + (mOffsetVertical / 4f);
            } else {
                positionY = mCurrentMoveEndY - TOP_PADDING - (((float) noteBitmap.getHeight()) / 6f) + (mOffsetVertical / 4f);
            }

            currentMatrixPositionX = (int) (positionX / mOffsetHorisontal);
            if (positionY >= 0) {
                currentMatrixPositionY = (int) (positionY / mOffsetVertical);
            } else {
                currentMatrixPositionY = 0;
            }
        }
        if (currentMatrixPositionX >= mLinesHorisontal) {
            currentMatrixPositionX = mLinesHorisontal - 1;
        }

        if (currentMatrixPositionY >= mLinesVertical) {
            currentMatrixPositionY = mLinesVertical - 1;
        }

        final int matrixX = currentMatrixPositionX;
        final int matrixY = currentMatrixPositionY;

        final Animation anim = AnimationUtils.loadAnimation(mContext, com.logtomobile.spen.R.anim.blink);
        final RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(mNoteWidth, mNoteHeight);
        if (showAnimations && matrixX != -1) {

            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            MyNoteRect rect = mRectangleMatrix[matrixX][matrixY];
            params.setMargins(rect.getLeft() + SCANVAS_LEFT_PADDING, rect.getTop(), 0, 0);

            anim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    //mNoteOverlay.setVisibility(View.GONE);
                    mNoteOverlay.setBackgroundColor(Color.TRANSPARENT);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

        }

        final Note.ChromaticScale finalChromaticScale = chromaticScale;

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if (SAVE_DRAWN_IMAGES) {
                    saveDrawnImage(noteBitmap);
                }
                if (showAnimations) {
                    mNoteOverlay.setLayoutParams(params);
                    mNoteOverlay.setBackgroundColor(mBackgroundColor);
                }
                if (finalChromaticScale != null && matrixX != -1 && matrixY != -1) {
                    Note note = new Note(STAVE_MAXIMUM_SCALE - matrixY, finalChromaticScale);
                    mCurrentNoteTrack.setNote(matrixX, note);
                }
                if (showAnimations) {
                    mNoteOverlay.startAnimation(anim);
                }
                mDrawingAreaStartX = -1;
                mDrawingAreaEndX = -1;
                mDrawingAreaStartY = -1;
                mDrawingAreaEndY = -1;
                invalidate();
            }
        });


    }

    private void setDrawEndingThread() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(THREAD_DELAY);
                    recognizaAndSetNote(true);
                } catch(InterruptedException ie) {
                    mCurrentDrawEndingThread = null;
                }
                mCurrentDrawEndingThread = null;
            }
        });

        thread.start();
        mCurrentDrawEndingThread = thread;

    }

    private void saveDrawnImage(Bitmap bitmap) {
        try{
            String directoryPath = Environment.getExternalStorageDirectory() + "/neural_network_samples";
            File imagesDir = new File(directoryPath);
            if (!imagesDir.exists()) {
                imagesDir.mkdirs();
            }
            int filesCount = 0;
            if (imagesDir.listFiles() != null) {
                filesCount = imagesDir.listFiles().length;
            }
            File f = new File(directoryPath + "/" + filesCount + ".png");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fo);

            fo.close();
        } catch (IOException ioe) {
            throw new NullPointerException();
        }

    }

    public void train(double[] param, final OnProgressChangedListener listener) {
        mRecognizer.addLearningSample(new LearningSample(mLastDrawnNote, param));
        mRecognizer.train(new OnProgressChangedListener() {
            @Override
            public void onProgressChanged(int progress, double error) {
                if (progress >= NetworkTrainer.MAX_EPOCH_NUMBER) {
                    try {
                        Serializer serializer = Serializer.getInstance();
                        serializer.save(mContext, mRecognizer.getLearningSequence(), Constants.LEARNING_SEQUENCE_FILENAME);
                    } catch (Exception e) {
                        Log.d("doszlo", "wyjątek w StaveMusicCanvas w train");
                    }

                }
                listener.onProgressChanged(progress, error);
            }
        });
    }

    public void saveNeuralNetwork(String filename) {
        Serializer serializer = Serializer.getInstance();
        try {
            serializer.saveToExternalStorage(mRecognizer.getNeuralNetwork(), filename);
        } catch (IOException ioe) {
            //no need to handle this exception
        }
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (!mIsEraserSet && !mIsDrawingDisabled) {
            super.onDraw(canvas);
        }
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStrokeWidth(5);
        paint.setStyle(Paint.Style.FILL);


        for (Note.NoteTrack track : mTrackMap.values()) {
            ColorFilter filter = new LightingColorFilter(Color.BLACK, track.getInstrumentType().getInstrumentColor(mContext));
            paint.setColorFilter(filter);
            paint.setAlpha(150);

            List<Note> notes = track.getNotes();
            Note note;
            int currentPlayedNote = mCurrentPlayedNotesMap.get(track.getInstrumentType());
            for (int i = 0 ; i < notes.size(); i++) {
                note = notes.get(i);
                int vertivalPosition = STAVE_MAXIMUM_SCALE - note.getTone();
                if ((((i < mDrawingAreaStartX || i > mDrawingAreaEndX) || (vertivalPosition < mDrawingAreaStartY || vertivalPosition > mDrawingAreaEndY)) || (mDrawingAreaStartX == -1)) && !note.equals(Note.INVALID_NOTE)) {
                    MyNoteRect noteImage = mRectangleMatrix[i][STAVE_MAXIMUM_SCALE - note.getTone()];
                    if (i == currentPlayedNote) {
                        paint.setAlpha(255);
                        canvas.drawBitmap(mNotesBitmapsMap.get(note.getChromaticScale()), noteImage.getLeft(), noteImage.getTop(), paint);
                        currentPlayedNote = -1;
                        paint.setAlpha(150);
                    } else {
                        canvas.drawBitmap(mNotesBitmapsMap.get(note.getChromaticScale()), noteImage.getLeft(), noteImage.getTop(), paint);
                    }
                }
            }
        }

        paint.setAlpha(255);
        paint.setColorFilter(null);

        List<Note> notes = mCurrentNoteTrack.getNotes();
        Note note;
        int currentPlayedNote = mCurrentPlayedNotesMap.get(mCurrentNoteTrack.getInstrumentType());
        for (int i = 0 ; i < notes.size(); i++) {
            note = notes.get(i);
            int vertivalPosition = STAVE_MAXIMUM_SCALE - note.getTone();
            if ((((i < mDrawingAreaStartX || i > mDrawingAreaEndX) || (vertivalPosition < mDrawingAreaStartY || vertivalPosition > mDrawingAreaEndY)) || (mDrawingAreaStartX == -1)) && !note.equals(Note.INVALID_NOTE)) {

                MyNoteRect noteImage = mRectangleMatrix[i][STAVE_MAXIMUM_SCALE - note.getTone()];
                if (i == currentPlayedNote) {
                    ColorFilter filter = new LightingColorFilter(Color.BLACK, mCurrentNoteTrack.getInstrumentType().getInstrumentColor(mContext));
                    paint.setColorFilter(filter);
                    canvas.drawBitmap(mNotesBitmapsMap.get(note.getChromaticScale()), noteImage.getLeft(), noteImage.getTop(), paint);
                    currentPlayedNote = -1;
                    paint.setColorFilter(null);
                } else {
                    canvas.drawBitmap(mNotesBitmapsMap.get(note.getChromaticScale()), noteImage.getLeft(), noteImage.getTop(), paint);
                }
            }
        }
    }


    /*private void drawMyNote(Canvas canvas, Paint paint, MyNoteObject myRectangle) {
        canvas.drawOval(myRectangle.getRectF(), paint);
        canvas.drawLine(myRectangle.getRight(), myRectangle.getRectF().centerY(), myRectangle.getRight(), myRectangle.getRectF().centerY() - (mOffsetVertical * 6), paint);

    }*/

    private boolean onEraserTouchEvent(View view, MotionEvent event) {
        if (event.getX() < mWidth && event.getX() > 0 && event.getY() < mHeight && event.getY() > 0) {

            int matrixPositionX = (int) (event.getX() / mOffsetHorisontal);
            Note note = mCurrentNoteTrack.getNotes().get(matrixPositionX);
            int staveOvalCenterY = TOP_PADDING + (int) ((STAVE_MAXIMUM_SCALE - note.getTone()) * mOffsetVertical);

            int noteTop;
            if (note.getChromaticScale() == Note.ChromaticScale.one) {
               noteTop = staveOvalCenterY - (int) mOffsetVertical;
            } else {
                noteTop = staveOvalCenterY - mNoteHeight;
            }
            int noteBottom = staveOvalCenterY + (int) mOffsetVertical;
            int noteLeft  = (int) ((matrixPositionX * mOffsetHorisontal) + ((mOffsetHorisontal - mNoteWidth) / 2));
            int noteRight;

            if (note.getChromaticScale() == Note.ChromaticScale.one || note.getChromaticScale() == Note.ChromaticScale.half || note.getChromaticScale() == Note.ChromaticScale.four) {
                noteRight = (int) (((matrixPositionX + 1) * mOffsetHorisontal) - ((mOffsetHorisontal - mNoteWidth) / 2) - mNoteWidth * (1f/3f));
            } else {
                noteRight = (int) (((matrixPositionX + 1) * mOffsetHorisontal) - ((mOffsetHorisontal - mNoteWidth) / 2));
            }
            //Log.d("doszlo", noteTop + " " + noteBottom + " " + event.getY());
            if (event.getY() < noteBottom && event.getY() > noteTop && event.getX() > noteLeft && event.getX() < noteRight) {
                mCurrentNoteTrack.setNote(matrixPositionX, Note.INVALID_NOTE);
                invalidate();
            }
        }
        return false;
    }

    public void enableDraw() {
        try {
            clearScreen();
        } catch (NullPointerException npe) {
            //nothing to do
        }
        destroyDrawingCache();
        invalidate();
        mIsDrawingDisabled = false;

        setOnTouchListener();
    }

    public void disableDraw() {
        mIsDrawingDisabled = true;
        setSPenTouchListener(null);
    }

    public void setOnTouchListener() {
        this.setSPenTouchListener(new SPenTouchListener() {
            @Override
            public boolean onTouchPen(View view, MotionEvent event) {
                if (mIsEraserSet) {
                    return onEraserTouchEvent(view, event);
                } else {
                    return onTouchEvent(view, event);
                }
            }

            @Override
            public boolean onTouchFinger(View view, MotionEvent event) {
                if (mIsEraserSet) {
                    return onEraserTouchEvent(view, event);
                } else {
                    return onTouchEvent(view, event);
                }
            }

            @Override
            public boolean onTouchPenEraser(View view, MotionEvent event) {
                if (mIsEraserSet) {
                    return onEraserTouchEvent(view, event);
                } else {
                    return onTouchEvent(view, event);
                }
            }

            @Override
            public void onTouchButtonDown(View view, MotionEvent motionEvent) {

            }

            @Override
            public void onTouchButtonUp(View view, MotionEvent motionEvent) {

            }
        });
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

    }


    private class MyBackgroundView extends View {
        private static final int LEFT_STAVE_OFFSET = 60;
        private int mLinesHorisontal;
        private int mLinesVertical;

        private int mHeight;
        private int mActiveAreaHeight;
        private int mWidth;

        private int mEdgeThickness;

        public MyBackgroundView(Context context, int linesHorisontal, int linesVertical, int edgeThickness, int lineThickness) {
            super(context);
            mLinesHorisontal = linesHorisontal;
            mLinesVertical = linesVertical;
            mEdgeThickness = edgeThickness;
            setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        }

        @Override
        public void draw(Canvas canvas) {
            mHeight = this.getHeight();
            mWidth = this.getWidth();
            mActiveAreaHeight = mHeight - TOP_PADDING - SCANVAS_BOTTOM_PADDING;

            Paint paintBlue = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintBlue.setColor(mContext.getResources().getColor(com.logtomobile.spen.R.color.stave_color));
            paintBlue.setStrokeWidth(5);

            Paint paintGray = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintGray.setColor(Color.GRAY);
            paintGray.setStrokeWidth(1);

            float offsetHorisontal = ((float) mWidth/(float) mLinesHorisontal);
            float offsetVertical = ((float) mActiveAreaHeight/(float) mLinesVertical);

            Paint transparentPaint = new Paint();
            transparentPaint.setColor(Color.TRANSPARENT);
            transparentPaint.setStyle(Paint.Style.FILL);
            canvas.drawRect(new Rect(0, 0, mWidth, mHeight), transparentPaint);

            //drawEdges(canvas, mEdgeThickness);

            canvas.drawRect(LEFT_STAVE_OFFSET * (1f/5f), TOP_PADDING + 3.5f * mOffsetVertical, LEFT_STAVE_OFFSET * (4f/5f), mHeight- SCANVAS_BOTTOM_PADDING - 3.5f * mOffsetVertical, paintBlue);

            canvas.drawLine(LEFT_STAVE_OFFSET, TOP_PADDING + 3.5f * mOffsetVertical, LEFT_STAVE_OFFSET, mHeight - SCANVAS_BOTTOM_PADDING - 3.5f * mOffsetVertical, paintBlue);

            canvas.drawBitmap(Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), com.logtomobile.spen.R.drawable.key), mActiveAreaHeight / 3, mActiveAreaHeight, false), LEFT_STAVE_OFFSET + 50, TOP_PADDING, paintBlue);
            int positionY;
            for (int i = 1; i < mLinesVertical; i += 2) {
                positionY = TOP_PADDING + (int) (offsetVertical * (i + 1f) - (offsetVertical / 2) );
                if (i > 1 && i < 12) {
                    canvas.drawLine(LEFT_STAVE_OFFSET, positionY, mWidth, positionY, paintBlue);
                } else {
                    canvas.drawLine(LEFT_STAVE_OFFSET, positionY, mWidth, positionY, paintGray);
                }
            }
            Paint paintLightBlue = new Paint(Paint.ANTI_ALIAS_FLAG);
            paintLightBlue.setColor(mContext.getResources().getColor(com.logtomobile.spen.R.color.stave_bottom_meter_color));
            paintLightBlue.setStrokeWidth(2);
            for (int i = 0; i < mLinesHorisontal; i++) {
                canvas.drawLine(SCANVAS_LEFT_PADDING + (i * mOffsetHorisontal), mHeight - 80, SCANVAS_LEFT_PADDING + (i * mOffsetHorisontal), mHeight, paintLightBlue);
            }
        }

        private void drawEdges(Canvas canvas, int edgeWidth) {
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(edgeWidth);

            int offset = (edgeWidth + 1) / 2;

            canvas.drawLine(offset, 0, offset, mHeight, paint);
            canvas.drawLine(mWidth - offset, 0, mWidth - offset, mHeight, paint);
            canvas.drawLine(0, offset, mWidth, offset, paint);
            canvas.drawLine(0, mHeight - offset, mWidth, mHeight - offset, paint);
        }
    }

    private class MyNoteRect {
        private int mLeft;
        private int mTop;

        public MyNoteRect(int left, int top) {
            mLeft = left;
            mTop = top;
        }

        public int getLeft() {
            return mLeft;
        }

        public int getTop() {
            return mTop;
        }
    }

}
