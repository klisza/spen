package com.logtomobile.spen;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by gwiazda on 29.12.13.
 */
public class MusicSamplesListActivity extends Activity {

    private TextView mTxtvTitle;
    private ListView mLvSamples;
    private List<MelodySaver.MusicSample> mMusicSamples;
    private boolean mStaveMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_music_samples);

        mTxtvTitle = (TextView) findViewById(R.id.txtvTitle);
        FontSetter.setFont(this, mTxtvTitle);
        mLvSamples = (ListView) findViewById(R.id.lvSamples);
        setListeners();

        Bundle extras = getIntent().getExtras();
        if (extras.containsKey(MainActivity.INTENT_EXTRA_CANVAS_MODE_STAVE)) {
            mStaveMode = extras.getBoolean(MainActivity.INTENT_EXTRA_CANVAS_MODE_STAVE);
        }

        loadSamples();
    }

    private void setListeners() {
        mLvSamples.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = MusicSamplesListActivity.this.getIntent();
                intent.putExtra("result", mMusicSamples.get(position));
                MusicSamplesListActivity.this.setResult(RESULT_OK, intent);
                finish();
            }
        });
        mLvSamples.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DialogFragment newFragment = new DeleteFileDialog(mMusicSamples.get(position).getFilePath(MusicSamplesListActivity.this));
                ft.add(newFragment, "save_dialog");
                ft.commit();
                return false;
            }
        });
    }

    private void loadSamples() {
        mMusicSamples = new ArrayList<MelodySaver.MusicSample>();

        if (mStaveMode) {
            List<MelodySaver.StaveMusicSample> staveSamples =  MelodySaver.getSavedStaveSamples(this);
            mMusicSamples.addAll(staveSamples);
        } else {
            List<MelodySaver.GridMusicSample> gridSamples =  MelodySaver.getSavedGridSamples(this);
            mMusicSamples.addAll(gridSamples);
        }

        mLvSamples.setAdapter(new MyAdapter(this, R.layout.lvitem, mMusicSamples));

    }

    private class MyAdapter extends ArrayAdapter<Object> {
        private List<MelodySaver.MusicSample> mValues;
        private LayoutInflater mInflater;
        private Context mContext;

        public MyAdapter(Context context, int resource, List<MelodySaver.MusicSample> values) {
            super(context, resource, values.toArray());
            mContext = context;
            mValues = values;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public View getView(final int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.lvitem, parent, false);
            }
            TextView textView = (TextView) convertView;
            FontSetter.setFont(mContext, textView);
            textView.setText(mValues.get(position).getName());
            return convertView;
        }
    }

    private class DeleteFileDialog extends DialogFragment {

        private String mFilePath;

        public DeleteFileDialog(String filePath) {
            super();
            mFilePath = filePath;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MusicSamplesListActivity.this);
            builder.setTitle("Removing sample");
            builder.setMessage("Are yoy sure you want to remove this sample?");

            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    File file = new File(mFilePath);
                    if (file.exists()) {
                        file.delete();
                    }
                    loadSamples();
                }
            });
            builder.setNegativeButton("Cancel", null);
            return builder.create();
        }
    }
}
